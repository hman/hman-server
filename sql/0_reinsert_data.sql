/* Populate tables */
TRUNCATE tag CASCADE;
TRUNCATE "work" CASCADE;
TRUNCATE work_cover CASCADE;
TRUNCATE work_tag CASCADE;
TRUNCATE work_details CASCADE;
TRUNCATE work_details_titles_translated CASCADE;
TRUNCATE work_personal_metadata CASCADE;
TRUNCATE online_resource CASCADE;

COPY tag(tag_id,created,name,namespace)
FROM '/tmp/data/tag.csv' DELIMITER ',' CSV HEADER;

COPY work(work_id,created,title)
FROM '/tmp/data/work.csv' DELIMITER ',' CSV HEADER;

COPY work_cover(hash,created,url,work_work_id)
FROM '/tmp/data/work_cover.csv' DELIMITER ',' CSV HEADER;

COPY work_tag(tag_id, work_id)
FROM '/tmp/data/work_tag.csv' DELIMITER ',' CSV HEADER;

COPY work_details(work_work_id)
FROM '/tmp/data/work_details.csv' DELIMITER ',' CSV HEADER;

COPY work_details_titles_translated(work_details_work_work_id, titles_translated)
FROM '/tmp/data/work_details_titles_translated.csv' DELIMITER ',' CSV HEADER;

COPY work_personal_metadata(last_read, rating, read_count, trash, work_work_id)
FROM '/tmp/data/work_personal_metadata.csv' DELIMITER ',' CSV HEADER;

COPY online_resource(online_resource_id, created, origin, pages, url, work_online_resource)
FROM '/tmp/data/online_resource.csv' DELIMITER ',' CSV HEADER;

REFRESH MATERIALIZED VIEW work_tag_mv;
REFRESH MATERIALIZED VIEW work_tag_concat_mv;
