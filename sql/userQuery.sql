SELECT work_id, gallery_owner, title, work_metadata_view.created, thumbnail_hash, thumbnail_url, concat_tags, online_urls,
	offline_resource_id, max_pages, last_read, rating, read_count, trash, favorite_groups,
	CASE /* Favorite count is needed to filter favorites INCLUDE, EXCLUDE, IGNORE */
      WHEN personal_join.favorite_groups = '{}' OR personal_join.favorite_groups IS NULL THEN 0::bigint
      ELSE array_length(personal_join.favorite_groups, 1)
    END AS favorites
FROM work_metadata_view
LEFT JOIN ( 
SELECT work_personal_metadata.work_personal_metadata_id, user_user_id, work_work_id,
    work_personal_metadata.last_read,
    work_personal_metadata.rating,
    work_personal_metadata.read_count,
    work_personal_metadata.trash,
    array_remove(array_agg(work_personal_metadata_favorite_groups.favorite_groups), NULL::character varying) AS favorite_groups
   	FROM work_personal_metadata
     LEFT JOIN work_personal_metadata_favorite_groups 
     ON work_personal_metadata_favorite_groups.work_personal_metadata_work_personal_metadata_id = work_personal_metadata.work_personal_metadata_id  
  	GROUP BY work_personal_metadata.work_personal_metadata_id
) AS personal_join ON work_metadata_view.work_id = personal_join.work_work_id
LEFT JOIN users ON users.user_id=user_user_id
WHERE (users.username='hman' OR user_user_id IS NULL)

LIMIT 25 
OFFSET 0



