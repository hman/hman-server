/* create frequency column */
ALTER TABLE public.work_tag ADD frequency int8 NULL ;

/* pre compute work tag join */
CREATE MATERIALIZED VIEW work_tag_mv AS
  SELECT work.work_id,
    tag.namespace,
    tag.name
   FROM work
     JOIN work_tag ON work.work_id = work_tag.work_id
     JOIN tag ON work_tag.tag_id = tag.tag_id
  ORDER BY work.work_id, tag.namespace, tag.name;

create index on work_tag_mv (work_id);
create index on work_tag_mv (namespace);
create index on work_tag_mv (name);

/* pre compute work concatenated tag join */
CREATE MATERIALIZED VIEW public.work_tag_concat_mv AS
 SELECT work_tag_mv.work_id,
    array_agg((LOWER(work_tag_mv.namespace::text) || ':'::text) || LOWER(work_tag_mv.name::text)) AS concat_tags,
    array_agg(LOWER(work_tag_mv."name"::text)) AS concat_tags_raw
   FROM work_tag_mv
  GROUP BY work_tag_mv.work_id
  ORDER BY work_tag_mv.work_id;