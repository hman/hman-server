CREATE OR REPLACE VIEW public.home_highest_rated_view AS
 SELECT work.work_id AS id,
    'WORK'::text AS container_type,
    work_personal_metadata.rating,
    users.username
   FROM work
     JOIN work_personal_metadata ON work.work_id = work_personal_metadata.work_work_id
     JOIN users ON work_personal_metadata.user_user_id = users.user_id
UNION
 SELECT gallery.gallery_id AS id,
    'GALLERY'::text AS container_type,
    gallery_personal_metadata.rating,
    users.username
   FROM gallery
     JOIN gallery_personal_metadata ON gallery.gallery_id = gallery_personal_metadata.gallery_gallery_id
     JOIN users ON gallery_personal_metadata.user_user_id = users.user_id
UNION
 SELECT collection.collection_id AS id,
    'COLLECTION'::text AS container_type,
    collection_personal_metadata.rating,
    users.username
   FROM collection
     JOIN collection_personal_metadata ON collection.collection_id = collection_personal_metadata.collection_collection_id
     JOIN users ON collection_personal_metadata.user_user_id = users.user_id
  ORDER BY 3 DESC;
  
  
  
CREATE OR REPLACE VIEW public.home_highest_read_count_view AS
 SELECT work.work_id AS id,
    'WORK'::text AS container_type,
    work_personal_metadata.read_count,
    users.username
   FROM work
     JOIN work_personal_metadata ON work.work_id = work_personal_metadata.work_work_id
     JOIN users ON work_personal_metadata.user_user_id = users.user_id
UNION
 SELECT gallery.gallery_id AS id,
    'GALLERY'::text AS container_type,
    gallery_personal_metadata.read_count,
    users.username
   FROM gallery
     JOIN gallery_personal_metadata ON gallery.gallery_id = gallery_personal_metadata.gallery_gallery_id
     JOIN users ON gallery_personal_metadata.user_user_id = users.user_id
UNION
 SELECT collection.collection_id AS id,
    'COLLECTION'::text AS container_type,
    collection_personal_metadata.read_count,
    users.username
   FROM collection
     JOIN collection_personal_metadata ON collection.collection_id = collection_personal_metadata.collection_collection_id
     JOIN users ON collection_personal_metadata.user_user_id = users.user_id
  ORDER BY 3 DESC;

CREATE OR REPLACE VIEW public.home_last_read_view AS
 SELECT work.work_id AS id,
    'WORK'::text AS container_type,
    work_personal_metadata.last_read,
    users.username
   FROM work
     JOIN work_personal_metadata ON work.work_id = work_personal_metadata.work_work_id
     JOIN users ON work_personal_metadata.user_user_id = users.user_id
UNION
 SELECT gallery.gallery_id AS id,
    'GALLERY'::text AS container_type,
    gallery_personal_metadata.last_read,
    users.username
   FROM gallery
     JOIN gallery_personal_metadata ON gallery.gallery_id = gallery_personal_metadata.gallery_gallery_id
     JOIN users ON gallery_personal_metadata.user_user_id = users.user_id
UNION
 SELECT collection.collection_id AS id,
    'COLLECTION'::text AS container_type,
    collection_personal_metadata.last_read,
    users.username
   FROM collection
     JOIN collection_personal_metadata ON collection.collection_id = collection_personal_metadata.collection_collection_id
     JOIN users ON collection_personal_metadata.user_user_id = users.user_id
  ORDER BY 3 DESC;
  
  
CREATE VIEW work_metadata_view AS
SELECT work.work_id,
       work.gallery_work AS gallery_owner,
       work.title,
       work.created,
       CASE
           WHEN calc_pages.max_pages IS NULL THEN 0::bigint
           ELSE calc_pages.max_pages::bigint
       END AS max_pages,
       work_cover.url AS thumbnail_url,
       CASE
           WHEN work_cover.hash IS NULL THEN '-1'::character varying
           ELSE work_cover.hash
       END AS thumbnail_hash,
       tags.concat_tags,
       calc_pages.urls AS online_urls,
       offline_resource.work_work_id AS offline_resource_id
FROM
WORK
LEFT JOIN /* Offline url */
	offline_resource ON WORK.work_id=offline_resource.work_work_id
LEFT JOIN /* Online urls and pages calculaton */
  (SELECT online_resource.work_online_resource AS work_id,
          max(online_resource.pages) AS max_pages,
          array_agg(online_resource.url) AS urls
   FROM online_resource
   GROUP BY online_resource.work_online_resource ) calc_pages ON work.work_id = calc_pages.work_id
LEFT JOIN /* Other metadata */ 
	work_details ON work.work_id = work_details.work_work_id
LEFT JOIN /* Thumbnail */ 
	work_cover ON work.work_id = work_cover.work_work_id
LEFT JOIN /* Collect tags */ 
	work_tag_concat_mv tags ON work.work_id = tags.work_id;
	
CREATE VIEW gallery_metadata_view AS
SELECT gallery_id, 
	gallery.created,gallery.title, url, 
	collection_gallery, array_agg(WORK.work_id)
FROM GALLERY
LEFT JOIN gallery_details ON gallery_id=gallery_details.gallery_gallery_id
LEFT JOIN gallery_cover ON gallery_id=gallery_cover.gallery_gallery_id
LEFT JOIN "work" ON gallery_id=WORK.gallery_work
GROUP BY gallery_id,url;

CREATE VIEW collection_metadata_view AS
SELECT collection_id, 
	collection.created,collection.title, url, array_agg(gallery.gallery_id)
FROM collection
LEFT JOIN collection_details 
ON collection_id= collection_details.collection_collection_id
LEFT JOIN collection_cover 
ON collection_id=collection_cover.collection_collection_id
LEFT JOIN "gallery" ON collection_id=gallery.collection_gallery
GROUP BY collection_id,url;

