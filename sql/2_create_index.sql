/* Indices */
CREATE INDEX IF NOT EXISTS work_created_idx ON work (created);
CREATE INDEX IF NOT EXISTS work_title_idx ON work (title);
CREATE INDEX IF NOT EXISTS gallery_created_idx ON gallery (created);
CREATE INDEX IF NOT EXISTS gallery_title_idx ON gallery (title);
CREATE INDEX IF NOT EXISTS collection_created_idx ON collection (created);
CREATE INDEX IF NOT EXISTS collection_title_idx ON collection (title);
CREATE INDEX IF NOT EXISTS tag_name_idx ON tag (name);
CREATE INDEX IF NOT EXISTS tag_namespace_idx ON tag (namespace);
CREATE INDEX IF NOT EXISTS work_cover_hash_idx ON work_cover (hash);
CREATE INDEX IF NOT EXISTS gallery_cover_hash_idx ON gallery_cover (hash);
CREATE INDEX IF NOT EXISTS collection_cover_hash_idx ON collection_cover (hash);
CREATE INDEX IF NOT EXISTS work_personal_metadata_last_read_idx ON work_personal_metadata (last_read);
CREATE INDEX IF NOT EXISTS work_personal_metadata_rating_idx ON work_personal_metadata (rating);
CREATE INDEX IF NOT EXISTS work_personal_metadata_read_count_idx ON work_personal_metadata (read_count);
CREATE INDEX IF NOT EXISTS work_personal_metadata_trash_idx ON work_personal_metadata (trash);
CREATE INDEX IF NOT EXISTS gallery_personal_metadata_last_read_idx ON gallery_personal_metadata (last_read);
CREATE INDEX IF NOT EXISTS gallery_personal_metadata_rating_idx ON gallery_personal_metadata (rating);
CREATE INDEX IF NOT EXISTS gallery_personal_metadata_read_count_idx ON gallery_personal_metadata (read_count);
CREATE INDEX IF NOT EXISTS gallery_personal_metadata_trash_idx ON gallery_personal_metadata (trash);
CREATE INDEX IF NOT EXISTS collection_personal_metadata_last_read_idx ON collection_personal_metadata (last_read);
CREATE INDEX IF NOT EXISTS collection_personal_metadata_rating_idx ON collection_personal_metadata (rating);
CREATE INDEX IF NOT EXISTS collection_personal_metadata_read_count_idx ON collection_personal_metadata (read_count);
CREATE INDEX IF NOT EXISTS collection_personal_metadata_trash_idx ON collection_personal_metadata (trash);
CREATE INDEX IF NOT EXISTS online_resource_created_idx ON online_resource (created);
CREATE INDEX IF NOT EXISTS online_resource_origin_idx ON online_resource (origin);
CREATE INDEX IF NOT EXISTS online_resource_pages_idx ON online_resource (pages);
CREATE UNIQUE INDEX IF NOT EXISTS work_tag_tag_id_work_id_idx ON work_tag (tag_id, work_id)