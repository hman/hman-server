# HMan Server v0.13

This server manages a general hierarchical archive structure with user metadata.
One example of how to use the server: use this structure to manage h-manga:

* Collection: Galleries grouped by some similarity. E.g. same gallery but different
language or different scanlator.
* Gallery: Grouping of multiple works into one gallery. E.g. a gallery most
likely consists of multiple chapters.
* Work: Most atomic unit for uploads. Single chapters of a gallery.

Another possibility is to use the server to manage h-movies:

* MovieCollection: HMovies and/or HMovieGalleries grouped by some similarity. E.g. same movie, but 
different language (dubbed, original)
* MovieGallery: Grouping of multiple HMovies, e.g. movies from the same series
* Movie: Most atomic unit for uploads. Single episodes of a series or a standalone movie.

These different hierarchies have to be implemented before they can be used.
My main focus will be these two hierarchies, but the system can be extended any time
in the future to manage more stuff.

## Changelog

See [full changelog](CHANGELOG.md).


# Public REST Api #

Main REST API endpoint is at `$hostname:6776/api`. From there on, each
api function can be explored.

Further documentation on how to use the api coming.

### Efficiency of Queries (old api, but result still relevant) ###

This server is designed to work with around 1.000.000 galleries and about 100.000 (namespaced) tags.
Personal metadata (ratings, favorites, read count etc.) is updated instantly, tag changes will take about 5 seconds per (batch) tag change.

Most search queries will take an insignificant amount of time to complete. 
On the mirrored ehentai database (~400.000 galleries, ~80.000 namespaced tags, average ~10 tags per gallery),
this quite complex query takes about a second to complete.

![](.wiki/v0.8-complex-query.png)

# Frontend #

In addition to a REST API, on server start-up a front-end is accessible via any browser supporting
javascript at the hostname on port `6776`.

## Browser v0.9.0 ##

Only login and home view is implemented, show some categories of works/galleries/collections.

![](.wiki/v0.10-web.png)

## Mobile ##

![](.wiki/v0.9-web-mobile.jpg)

# Developer Notes #

This project consists of:

* A REST metadata server
* An embedded html + javascript front-end
  * html is templated using *thymeleaf*
* A JavaFX viewer with advanced editing features for the REST Api 

**Special Dependencies**:

* This project uses [Lombok](https://projectlombok.org)
  
**Additional Preparation**:

Your IDE of choice should be supported by Lombok. 
[NetBeans](https://projectlombok.org/setup/netbeans), 
[IntelliJ IDEA](https://projectlombok.org/setup/intellij), 
and [Eclipse](https://projectlombok.org/setup/eclipse) are the most prominent IDEs for Java. 

Choose one IDE and follow the instructions in the link given for your IDE.
For other IDEs, visit the [Lombok](https://projectlombok.org) homepage.

## Project Setup ##

* Install and setup lombok and postgresql properly
  * For IntelliJ: install lombok plugin and enable annotation processing
  * You can test the database with a database manager, e.g. `dbeaver` before
  continuing
* Clone the repository
* Assemble the project via `gradle assemble` to check the prerequisites or
use the bundled gradle wrapper.
* The project expects a postgres db `hman` at localhost with username 
`postgres` and password `123`. You can change the defaults in the 
`application.yml` file.
* Once the database is running, the schema has to be created once.
To do this, change `hibernate.ddl-auto` to `create` in the `application.yml`
file.
* Build the project.
* Run the project once.
* The schema should now be created. Stop the program and change 
`hibernate.ddl-auto` to `update`. Otherwise, the database is cleaned every
time the program is restarted.
* The api is user protected. Create your development user in the
`users` table in the database with the role `ADMIN` in the `user_role` table.
* You should now be able to navigate the REST api at `/api` with that user.
* You are ready for development!

# How to Contribute #

This is a large project. Depending on your area of expertise, you can
contribute to different parts of it.

If you want to implement an issue, search for the issue number in the source code.

## You Are a DB and/or a JPA Guy ##

Let JPA generate the database by starting the application once.

* JPA: Every relation is modeled in the `jpamodel` package. Additions will
not break anything. Editing existing attributes will require fixing things 
in `repositories` and `hateoas` packages. Most likely the changes have to be implemented
in the web fronted package `web` as well.

* DB: After JPA has generated the database, you can toy with it however you
want. Implementing your changes or efficient SQL queries requires someone 
who knows JPA or how data access via JDBC templates works, though.

## You Are a Javascript Web-Frontend Guy ###

To consume the REST API, explore it first from the root at `/api`.
The rel types are fixed for a major version. Inform yourself how to consume
a REST API which provides HAL JSON responses.

The basic structure will be:

* `/api`: The root
* `/api/archives`: Everything related to archives
* `/api/pendingChanges`: Everything related to the modification of archives by users

### Contribute to the existing frontend ### 

The frontend is built with Thymeleaf HTML templates combined with basic
jQuery ajax requests. Some points of references where you can contribute:

* [CSS styling](src/resources/static/css): Everything related to styling
* [Javascript](src/resources/static/js): Everything related to how the
REST api is consumed 
* [HTML Templates](src/resources/templates): Thymeleaf HTML templates with inlined
javascript

## You Are an Architect Guy ##

* Some suggestions for the current architecture? 
* Introduced some cyclic dependencies by mistake? 
* Inheritance structure is not good? 
* Package-by-feature was a mistake? 
* HATEOAS is too much bloat?

Feel free to provide suggestions or refactor code yourself. If you do, try to not create
cyclic dependencies. Neither between classes, nor between packages. A notable exception
is JPA bidirectional relationships, cycles are a design decision and needed if one
wants to traverse the relationship in both ways.

![](.wiki/v0.12-dsm.png)

## You Are a Security Guy ##

Everything security related is in the `security` package. The api is spring
authentication secured (both basic auth and form input for the web-frontend)
and appropriate errors are thrown if not logged in. Additionally, 
 since the authentication is implied in all controllers, users can only
 retrieve their personal metadata (e.g. ratings, favorites). This should not change, but I'm open to 
 suggestions.
 
## You Are a Big Data Guy ##

You want to analyse your big collection and insert it into the db in one go? 
One sample can be found and changed in the `dbdata` package 
(ehen database inserted and crudely analyzed on the fly).

I also look into the possibility of training a tagging mechanism to pre tag unknown galleries
with deep learning. Someone with knowledge on deep learning is always appreciated.