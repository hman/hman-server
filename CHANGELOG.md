## v0.13 (scheduled)

* HATEOAS links implementation
* Web fronted ported to archive system

## v0.12

* Simplified database model to allow for a general hierarchical archive model
  * Opens up the possibility to manage movies (with chapters), webms, etc.
  * Reduced source code drastically
* Voting system for users, moderators, and admins to vote on changes
* Most HATEOAS links are not implemented yet

## v0.11

* Security implemented. API can be accessed with basic authentication and 
form authentication, frontend uses only form authentication
* Custom error pages, html error for browsers, json for api consumers
* Mostly code refactoring to prevent technical debt in the future
* HATEOAS REST api

## v0.10

* Collection upload implemented, currently static save location (/srv/hman/resource/)
* Refactored javascript
* Implemented work viewer controlled with mouse, keyboard arrows, or 'a' and 'd'

## v0.9

* Abandoned Vaadin for Thymeleaf. Vaadin was too unpredictable. Everyone knowing
javascript can now participate in developing the front-end, as it is essentially
html templates with javascript (currently only bootstrap and jQuery are used)
* Restructured packages for future-proof structure. Fixed cycles in dependencies
where possible
* Package structure now adheres to the package-by-feature principle
* Changed server to manage multiple users with possible registrations

## v0.8.1

* Vaadin web front end for simple searches

## v0.8.0

* Abandoned Criteria API and QueryDSL for JDBC templates for main query
* Query performance tuning. Trade-off between update-ability and performance. Only tags are materialized for now.
* API somewhat stable

## v0.6.0

* .txt argument to create queries at startup
* global error handler
* image hash calculation
* fixed favorite tag duplication inside db
* Refactored code
