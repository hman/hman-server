function loadCollectionContent(id, type, key, suffix, username){
    console.info("ajax metadata /collection/"+id);

    $.ajax({url: "/collection/"+id, success: function(result){
        loadMetadataContent(id,type,key,suffix,result);
        loadUserContent(id,type,key,suffix,username);

        $("#image-dummy-"+suffix+key).attr("src", result["thumbnailUrl"]);
        $("#background-"+suffix+key).attr("src", result["thumbnailUrl"]);

        if(result["readCount"] === null || result["readCount"] === 0) {
            $("#unread-"+suffix+key).addClass("unread-marker");
        } else {
        }
        if(result["favorites"] !== null && result["favorites"] > 0)
            $("#favorite-"+suffix+key).addClass("favorite-marker glyphicon glyphicon-heart");

        //new status
        var newDate = result["created"];
        var currentDate = new Date().getTime();
        var diff = currentDate-newDate;
        if(Math.ceil(diff / (1000 * 3600 * 24)) < 8) {
            $("#new-"+suffix+key).addClass("new-marker");
        }


        //fetch tags and other stuff
        var galleries = result["galleries"];

        var ajaxRequests = [];
        var results = [];


        for (var i=0; i < galleries.length ; i++ ) {
            ajaxRequests.push( $.ajax("/gallery/"+galleries[i]).done(function (result) { results.push(result); }) );
        }
        //process when all works are fetched
        $.when.apply($, ajaxRequests).then(function() { collectionGalleryCallback(id,type,key,suffix,results); });
    }});
}

function collectionGalleryCallback(id, type, key, suffix, results) {
    console.log(results.length + " galleries fetched!");

    var ajaxRequests2 = [];
    var workResults = [];

    for (var i=0; i < results.length ; i++ ) {
        for(var j=0; j<results[i]["works"].length; j++){
            ajaxRequests2.push( $.ajax("/work/"+results[i]["works"][j]).done(function (result) { workResults.push(result); }) );
        }
    }

    //process when all works are fetched
    $.when.apply($, ajaxRequests2).then(function() { collectionWorkCallback(id,type,key,suffix,workResults); });
}

function collectionWorkCallback(id, type, key, suffix, results) {
    console.log(results.length + " works fetched");

    var concatTags = [];
    var onlineUrls = [];
    var pages = 0;
    for (var i = 0; i < results.length; i++) {
        concatTags = concatTags.concat(results[i]["tags"]);
        onlineUrls = onlineUrls.concat(results[i]["onlineUrls"]);
        if(results[i]["pages"] > pages) {
            pages = results[i]["pages"];
        }
    }

    $("#pages-"+suffix+key).html(pages);

    var languageTags = getLanguageTags(concatTags);
    if(languageTags.length !== 0) {
        if(languageTags.length > 1) {
            $("#language-"+suffix+key).addClass("lang-xs");
        } else {
            $("#language-"+suffix+key).addClass("lang-xs").attr("lang",languageTags[0]);
        }
    }
}