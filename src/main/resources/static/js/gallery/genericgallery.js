function updateRating(id, type, key, suffix, rating){
    for(var i = 1;i <= 5;i++){
        $("#star"+i+"-" + suffix + key).attr("style", "color: whitesmoke");
    }

    if (rating !== null) {
        if (rating > 0)
            $("#star1-" + suffix + key).attr("style", "color: yellow");
        if (rating > 1)
            $("#star2-" + suffix + key).attr("style", "color: yellow");
        if (rating > 2)
            $("#star3-" + suffix + key).attr("style", "color: yellow");
        if (rating > 3)
            $("#star4-" + suffix + key).attr("style", "color: yellow");
        if (rating > 4)
            $("#star5-" + suffix + key).attr("style", "color: yellow");
    }
}

function setStarFunction(id,type,key,suffix,username,rating) {
    $("#star"+rating+"-"+suffix+key).click(function () {
        console.log("clicked on star! "+rating);
        $.ajax({url: "/"+type+"/"+id+"/user/"+username,
            method: 'PUT',
            contentType: 'application/json',
            processData: false,
            data: JSON.stringify({
                rating: rating
            }),
            success: function() {
                updateRating(id, type, key, suffix, rating);
            }});
    });
}

function loadTagsAndPages(id,type,key,suffix,tags,pages) {
    $("#pages-"+suffix+key).html(pages);

    //if more than one language, set 'multi-language'
    var languageTags = getLanguageTags(tags);
    if(languageTags.length !== 0) {
        if(languageTags.length > 1) {
            $("#language-"+suffix+key).addClass("lang-xs");
        } else {
            $("#language-"+suffix+key).addClass("lang-xs").attr("lang",languageTags[0]);
        }
    }

}


function loadMetadataContent(id, type, key, suffix, result){
    var title = $("#title-"+suffix+key);
    title.html(result["title"]);
    title.attr("href", "/view/"+type+"/"+id);

    console.warn("Loading metadata content "+type);

    $("#description-"+suffix+key).html(result["title"]);
    $("#link-"+suffix+key).prop("href", "/view/"+type+"/"+id);
    $("#image-"+suffix+key).attr("src", result["thumbnailUrl"]);

    //new status
    var newDate = result["created"];
    var currentDate = new Date().getTime();
    var diff = currentDate-newDate;
    if(Math.ceil(diff / (1000 * 3600 * 24)) < 8) {
        $("#new-"+suffix+key).addClass("new-marker");
    }
}

function loadUserContent(id, type, key, suffix, username){
    $.ajax({url: "/"+type+"/"+id+"/user/"+username, success: function(result) {
        //rating
        var rating = result["rating"];

        updateRating(id, type, key, suffix, rating);
        setStarFunction(id ,type, key, suffix, username, 1);
        setStarFunction(id ,type, key, suffix, username, 2);
        setStarFunction(id ,type, key, suffix, username, 3);
        setStarFunction(id ,type, key, suffix, username, 4);
        setStarFunction(id ,type, key, suffix, username, 5);


        if(result["readCount"] === null || result["readCount"] === 0) {
            $("#unread-"+suffix+key).addClass("unread-marker");
        }

        if(result["favoriteGroups"] !== null && !result["favoriteGroups"].empty())
            $("#favorite-"+suffix+key).addClass("favorite-marker glyphicon glyphicon-heart");

        var trash = result["trash"];
        if(trash){
            $("#image-"+suffix+key).addClass("trash");
            $("#title-"+suffix+key).addClass("trash");
        }
    }, error: function() {
        setStarFunction(id ,type, key, suffix, username, 1);
        setStarFunction(id ,type, key, suffix, username, 2);
        setStarFunction(id ,type, key, suffix, username, 3);
        setStarFunction(id ,type, key, suffix, username, 4);
        setStarFunction(id ,type, key, suffix, username, 5);
        $("#unread-"+suffix+key).addClass("unread-marker");
    }
    });
}
