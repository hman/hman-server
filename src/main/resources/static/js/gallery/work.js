function loadWorkContent(id, type, key, suffix, username){
    $.ajax({url: "/work/"+id, success: function(result){
        loadMetadataContent(id,type,key,suffix,result);

        loadUserContent(id, type, key, suffix, username);

        loadTagsAndPages(id,type,key,suffix,result["tags"],result["pages"]);
    }});
}

