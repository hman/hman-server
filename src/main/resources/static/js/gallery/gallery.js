function loadGalleryContent(id, type, key, suffix, username){
    $.ajax({url: "/gallery/"+id, success: function(result){

        loadMetadataContent(id,type,key,suffix,result);
        loadUserContent(id,type,key,suffix,username);

        //fetch tags and other stuff
        var works = result["works"];

        var ajaxRequests = [];
        var results = [];

        function completeCallback() {
            var concatTags = [];
            var onlineUrls = [];
            var pages = 0;
            for (var i = 0; i < results.length; i++) {
                concatTags = concatTags.concat(results[i]["tags"]);
                onlineUrls = onlineUrls.concat(results[i]["onlineUrls"]);
                if(results[i]["pages"] > pages) {
                    pages = results[i]["pages"];
                }
            }

            loadTagsAndPages(id,type,key,suffix,concatTags,pages);
        }

        for (var i=0; i < works.length ; i++ ) {
            ajaxRequests.push( $.ajax("/work/"+works[i]).done(function (result) { results.push(result); }) );
        }
        //process when all works are fetched
        $.when.apply($, ajaxRequests)
            .then(function() {
                completeCallback();
            });
    }});
}

