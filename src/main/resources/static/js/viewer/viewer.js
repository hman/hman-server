function getNextPage() {
    if(currentlength - 1 > currentPage ) {
        currentPage++;
    } else {
        currentPage = 0;
        var index = sortedChapters.indexOf(currentChapter);
        if(index === -1) return;

        currentChapter = sortedChapters[index+1];
        currentWork = chapterMap[currentChapter];
        currentlength = fileNameMap[currentWork].length;
    }
    return baseMap[currentWork] + fileNameMap[currentWork][currentPage];
}

function getPrevPage() {
    if(currentPage > 0 ) {
        currentPage--;
    } else {
        console.log("Go back to prev chapter!");

        var index = sortedChapters.indexOf(currentChapter);
        if(index === -1) return;

        currentChapter = sortedChapters[index-1];
        currentWork = chapterMap[currentChapter];
        currentlength = fileNameMap[currentWork].length;
        currentPage = currentlength-1;
    }

    return baseMap[currentWork] + fileNameMap[currentWork][currentPage];
}
