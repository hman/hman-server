function createNoty(message, type) {
    var html = '<div class="alert alert-' + type + ' alert-dismissable page-alert">';
    html += '<button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>';
    html += message;
    html += '</div>';
    $(html).hide().prependTo('#noty-holder').slideDown();
}

function getLanguageTags(tags) {
    var languageTags = [];
    if(tags === null) return languageTags;

    if (tags.indexOf("language:english") !== -1) {
        languageTags.push("en");
    }
    if (tags.indexOf("language:japanese") !== -1) {
        languageTags.push("ja");
    }
    if (tags.indexOf("language:chinese") !== -1) {
        languageTags.push("zh");
    }
    if (tags.indexOf("language:dutch") !== -1) {
        languageTags.push("nl");
    }
    if (tags.indexOf("language:french") !== -1) {
        languageTags.push("fr");
    }
    if (tags.indexOf("language:german") !== -1) {
        languageTags.push("de");
    }
    if (tags.indexOf("language:hungarian") !== -1) {
        languageTags.push("hu");
    }
    if (tags.indexOf("language:italian") !== -1) {
        languageTags.push("it");
    }
    if (tags.indexOf("language:korean") !== -1) {
        languageTags.push("ko");
    }
    if (tags.indexOf("language:polish") !== -1) {
        languageTags.push("pl");
    }
    if (tags.indexOf("language:portuguese") !== -1) {
        languageTags.push("pt");
    }
    if (tags.indexOf("language:russian") !== -1) {
        languageTags.push("ru");
    }
    if (tags.indexOf("language:spanish") !== -1) {
        languageTags.push("es");
    }
    if (tags.indexOf("language:thai") !== -1) {
        languageTags.push("th");
    }
    if (tags.indexOf("language:vietnamese") !== -1) {
        languageTags.push("vi");
    }
    return languageTags
}

function retrieveFragment(url, replace) {
    $.get(url, function(data) {
        $(replace).html(data);
    });
}

