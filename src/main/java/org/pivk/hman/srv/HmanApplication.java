package org.pivk.hman.srv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @since 0.1
 */
@SpringBootApplication
//@EnableScheduling
//@EnableAsync
//@EnableRetry
public class HmanApplication {

	public static void main(String[] args) {
		SpringApplication.run(HmanApplication.class, args);
	}

}