package org.pivk.hman.srv.security;

import lombok.SneakyThrows;
import org.pivk.hman.srv.exception.ResourceException;
import org.springframework.core.annotation.Order;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @since 0.11
 */
@Order(0)
public class LoggingHandlerExceptionResolver
implements HandlerExceptionResolver {

    /**
     * @since 0.12
     */
    @SneakyThrows //FIXME #5 how to handle sendError IO exception?
    public ModelAndView resolveException(
            HttpServletRequest aReq, HttpServletResponse aRes,
            Object aHandler, Exception anExc
    ) {
        ModelAndView model = new ModelAndView("forward:/error");

        if(anExc instanceof ResourceException){
            aRes.sendError(((ResourceException) anExc).getHttpStatus().value());
        }
        else if(anExc instanceof ResourceNotFoundException){
            aRes.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        else {
            model.addObject("status",403);
            aRes.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }

        model.addObject("userMessage",anExc.getMessage());
        return model;
    }
}