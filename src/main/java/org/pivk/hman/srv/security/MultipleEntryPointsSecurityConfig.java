package org.pivk.hman.srv.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

/**
 * @since 0.11
 */
@Configuration
@EnableWebSecurity //TODO #6 encrypt user passwords
public class MultipleEntryPointsSecurityConfig {

    @Autowired JdbcTemplate jdbcTemplate;

    @Bean
    public UserDetailsService userDetailsService(){
        JdbcDaoImpl jdbcImpl = new JdbcDaoImpl();
        jdbcImpl.setDataSource(jdbcTemplate.getDataSource());
        jdbcImpl.setUsersByUsernameQuery("select username,password,enabled from users where username=?");
        jdbcImpl.setAuthoritiesByUsernameQuery(
                "select b.username, a.role from user_role a, users b where b.username=? and a.user_role_user=b.user_id"
        );

        return jdbcImpl;
    }

    @Bean(name="passwordEncoder")
    public PasswordEncoder passwordencoder(){
        return new BCryptPasswordEncoder();
    }

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable().antMatcher("/api/**").authorizeRequests().anyRequest().authenticated().and().httpBasic();

            http.authorizeRequests().antMatchers("/console/**").permitAll();
            http.headers().frameOptions().sameOrigin();
            http.csrf().disable();
        }

        /* To allow Pre-flight [OPTIONS] request from browser */
        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        }
    }

    @Configuration
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().
                authorizeRequests().antMatchers("/api/**").authenticated()
                .and().headers().frameOptions().sameOrigin()
                .and()
                .formLogin()
                .loginPage("/home").loginProcessingUrl("/home")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new Http401AuthenticationEntryPoint("val"))
                .accessDeniedHandler(new AccessDeniedHandlerImpl());
        }

        /* To allow Pre-flight [OPTIONS] request from browser */
        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        }
    }

    @Bean
    HandlerExceptionResolver customExceptionResolver () {
        return new LoggingHandlerExceptionResolver();
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect(){
        return new SpringSecurityDialect();
    }
}

