package org.pivk.hman.srv.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	/**
	 * @since 0.12
	 */
	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Sorting property does not exist.")
	@ExceptionHandler(PropertyReferenceException.class)
	public void handleSortingWrong(Exception ex){}
}