package org.pivk.hman.srv.dbdata;

import com.pettt.hman.metadata.converter.MetadataJsonConverter;
import com.pettt.hman.metadata.converter.beans.ESupportedSites;
import com.pettt.hman.metadata.converter.beans.MetadataBean;
import com.pettt.hman.metadata.converter.beans.MetadataTag;
import com.pettt.hman.metadata.converter.util.Util;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static com.pettt.hman.metadata.converter.RequestUrlConverter.ehentaiIdToGalleryUrl;
import static com.pettt.hman.metadata.converter.beans.ENamespace.*;
import static org.pivk.util.Util.*;

/**
 * A routine to convert scraped metadata into the db data scheme.
 * <p>
 *     Has to be updated manually after each DB change!
 */
@SuppressWarnings("SynchronizeOnNonFinalField") //print writers are only initialized one time, references do not change
@Slf4j
public class InsertData {
    private final Set<String> IGNORE_TAGS = new HashSet<>();

    private final String RESOURCE_SERVER_PATH = "/srv/hman/resource";

    private AtomicLong progress = new AtomicLong(0);
    private AtomicLong warnings = new AtomicLong(0);
    private AtomicLong errors = new AtomicLong(0);
    private AtomicLong ignored = new AtomicLong(0);

    private int fileCount = 0;

    private PrintWriter workWriter;
    private PrintWriter tagWriter;
    private PrintWriter workTagWriter;
    private PrintWriter workCoverWriter;
    private PrintWriter onlineResourceWriter;

    private Map<MetadataTag, Long> tagIdMap = new HashMap<>(70000);

    public static void main(String[] args) throws Exception {
        InsertData data = new InsertData();
        data.run(args);
    }

    private InsertData() {
        Collections.addAll(IGNORE_TAGS,"scat","guro","rape","yaoi"
                ,"futanari","non-h","insect","birth","torture","shemale","bbw","huge breasts","frottage");
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void run(String... args) throws Exception {
        //look for scraped path argument
        String path = "";
        boolean silent = false;
        for (String arg : args) { //TODO currently only ehen supported
            if(arg.contains("scraped/ehen/json/")) path = arg;
            if(arg.equalsIgnoreCase("--silent-csv")) silent = true;
        }

        if(path.isEmpty()) return;

        File resFolder = new File(RESOURCE_SERVER_PATH);

        if(!resFolder.exists())
            throw new IOException("Resource folder does not exist! " +
                    "Create it before running the server at "+ RESOURCE_SERVER_PATH);

        if(!resFolder.isDirectory())
            throw new IOException("Resource folder is not a directory! " +
                    "Create it before running the server at "+ RESOURCE_SERVER_PATH);

        if(!resFolder.canRead())
            throw new IOException("Resource folder has no read permission! " +
                    "Check permissions at "+ RESOURCE_SERVER_PATH);

        log.warn("------------------------------------------------------------------------------");
        log.warn("Found scraped ehen argument! Populating database with scraped ehentai metadata and thumbnails, if any.");

        if(!silent){ //get user ack
            {
                Scanner scanner = new Scanner(System.in);
                String input;
                do {
                    System.err.println("Overriding all local csv files! Continue? (y/n)");
                    input = scanner.next();
                    if(input.equalsIgnoreCase("n")) {
                        log.info("Aborting database data creation. Continue program execution.");
                        return;
                    }
                } while(!(input.equalsIgnoreCase("n") || input.equalsIgnoreCase("y")));
            }
        }

        definePrinters(); //overwrites old csv!

        long start = System.nanoTime();

        File f = new File(path);
        if(!(f.exists() && f.isDirectory() && f.canRead())) {
            log.error("Could not find or read metadata folder: {}", f.getPath());
            throw new IOException("File not found");
        }

        File[] files = f.listFiles();
        if(files == null) throw new IOException("Could not list files of "+path);

        fileCount = files.length;

        //read in passes because every work as a metadata bean wont fit into memory most likely
        //whereas only all tags will
        log.warn("------------------------------------------------------------------------------");
        log.warn("First pass: generate tag.csv");

        generateTags(files);

        parseTagsCsv(new File("tag.csv"));

        log.warn("Second pass: generate work.csv and relations");

        generateWorksAndRelation(files);

        long end = System.nanoTime();
        log.warn("Finished creating csv files in {} seconds", (end - start) / 1000000000.0);
        log.warn("------------------------------------------------------------------------------");
    }

    //creates print wirters to specified files, overwriting the file
    private void definePrinters() throws FileNotFoundException {
        workWriter = writer("archive.csv", "archive_id, created, ordering, title, archive_archive_id, type");
        tagWriter = writer("tag.csv", "tag_id, created, name, namespace");
        workTagWriter = writer("archive_tag.csv", "tag_id, archive_id");
        workCoverWriter = writer("cover.csv", "hash, created, url, archive_archive_id");
        onlineResourceWriter
              = writer("online_resource.csv", "online_resource_id, created, origin, pages, url, archive_online_resource");
    }
    private void flushAndClosePrinters() {
        workWriter.flush(); workWriter.close();
        tagWriter.flush(); tagWriter.close();
        workTagWriter.flush(); workTagWriter.close();
        workCoverWriter.flush(); workCoverWriter.close();
        onlineResourceWriter.flush(); onlineResourceWriter.close();
    }

    //parses the generated tags csv and creates beans to store them in the memory
    //metadata beans are then used to create work relations
    private void parseTagsCsv(File tagFile) throws IOException {
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(tagFile))){
            while ((line = reader.readLine()) != null) {
                try{
                    // use comma as separator
                    String[] tag = line.split(",");
                    if(!tag[0].equalsIgnoreCase("tag_id")) {
                        tagIdMap.put(new MetadataTag(tag[3], tag[2]), Long.valueOf(tag[0]));
                    }
                } catch (Exception e){
                    log.error("Error converting line {}",line);
                    log.error("{}", e.getMessage());
                    errors.incrementAndGet();
                }
            }
        }

        log.info("Finished reading tag.csv, found {} tags", tagIdMap.size());
    }

    //parses all given files for a metadata bean
    //TODO currently ehentai only, switch to auto-detect later
    //for each parsed bean the given action is applied
    private void parseData(File[] files, Consumer<MetadataBean> action) throws IOException {
        log.warn("Reading {} metadata files", files.length);

        for (File metadata : files) {
            String body = Util.readBody(metadata);
            try {
                MetadataBean bean = MetadataJsonConverter.jsonToBean(body, ESupportedSites.EHENTAI_ORG);
                action.accept(bean);
            } catch (Exception e){
                log.error("Could not parse metadata: {}",e.getMessage());
                errors.incrementAndGet();
            }

            Long i = progress.incrementAndGet();
            if(i %1000 == 0) {
                log.info("({}%) Parsed {} metadata files, {} errors, {} warnings",
                        (int) ((i/(double) fileCount) * 100),
                        i, errors.get(), warnings.get());
                flushPrinters();
            }
        }
    }

    private void generateWorksAndRelation(File[] fileList) throws IOException, InterruptedException {
        log.warn("Generating works and relations");
        final AtomicLong atomicWorkId = new AtomicLong(1);
        progress.set(0);
        warnings.set(0);
        errors.set(0);

        //TODO instead of split in equal size, define method
        //TODO to split into 8 equal parts (i.e. 8 threads)
        List<File[]> t = splitArrayIntoThreads(fileList, 7);

        List<Thread> threads = new ArrayList<>();

        for (File[] files : t) {
            Runnable r = () -> {
                try {
                    parseData(files, (bean -> { //parse bean into work
                        //reserve id
                        try{

                        Long workId = atomicWorkId.getAndIncrement();

                        List<String> translatedTitles = new ArrayList<>();
                        String mainTitle = "Unknown Title";
                        MetadataTag engTitle = bean.findFirstByNamespace(TITLE_ENG.db);
                        MetadataTag jpnTitle = bean.findFirstByNamespace(TITLE_JPN.db);
                        if (jpnTitle != null && jpnTitle.getTag() != null && !jpnTitle.getTag().isEmpty()) {
                            translatedTitles.add("jpn:" + jpnTitle.getTag());
                            mainTitle = jpnTitle.getTag();
                        }
                        if (engTitle != null && engTitle.getTag() != null && !engTitle.getTag().isEmpty()) {
                            translatedTitles.add("eng:" + engTitle.getTag());
                            mainTitle = engTitle.getTag();
                        }

                        //work
                        String work = String.format("%s,%s,,%s,,UNKNOWN", workId, toDbDate(), toDbString(mainTitle));

                        //work_details_titles_translated
                        StringBuilder translatedTitlesString = new StringBuilder();
                        for (String translatedTitle : translatedTitles) {
                            String translated = String.format("%s,%s", workId, toDbString(translatedTitle));
                            translatedTitlesString.append(translated).append("\n");

                        }

                        //work_cover
                        Long gid = Long.valueOf(bean.findFirstByNamespace(EHENTAI_GID.db).getTag());
                        String thumbnailPath = RESOURCE_SERVER_PATH + "/thumbnail/ehen/"
                                + (gid % 1000) + "/" + gid + ".jpg";

                        StringBuilder thumbString = new StringBuilder();
                        try {
                            BufferedImage img = ImageIO.read(new File(thumbnailPath));
                            String resUrl = "/resource/thumbnail/ehen/" + (gid % 1000) + "/" + gid + ".jpg";
//                            String thumb = String.format("%s,%s,%s,%s", ImageUtil.calculateHash(img), toDbDate(), resUrl, workId);
                            String thumb = String.format("%s,%s,%s,%s", "-1", toDbDate(), resUrl, workId);
                            thumbString.append(thumb).append("\n");
                        } catch (Exception e) {
                            log.error("Could not create thumbnail for work {} at {}: {}",
                                    workId, thumbnailPath, e.getMessage());
                        }


                        StringBuilder workTagRelation = new StringBuilder();
                        //tag relations
                        for (MetadataTag tag : bean.findAllRelationalMetadata()) {
                            if (IGNORE_TAGS.contains(tag.getTag().toLowerCase()))
                                throw new WorkIgnoredException(tag.getTag() + ": Ignored " + mainTitle);
                            Long id = tagIdMap.get(tag);
                            if (id != null) {
                                workTagRelation.append(id).append(",").append(workId).append("\n");
                            } else {
                                log.error("Could not find tag ID! {}", tag);
                            }
                        }


                        //online resource
                        MetadataTag gidString = bean.findFirstByNamespace(EHENTAI_GID.db);
                        MetadataTag tokenString = bean.findFirstByNamespace(EHENTAI_TOKEN.db);
                        String resourceString = workId + ","
                                + toDbDate(new Date().getTime()) + "," +
                                "e-hentai.org," + bean.findFirstByNamespace(PAGES.db).getTag() + ","
                                + ehentaiIdToGalleryUrl(gidString.getTag(), tokenString.getTag()) + "," + workId;

                        synchronized (workWriter) {
                            workWriter.println(work);
                        }
                        synchronized (workCoverWriter) {
                            workCoverWriter.print(thumbString.toString());
                        }
                        synchronized (workTagWriter) {
                            workTagWriter.print(workTagRelation.toString());
                        }
                        synchronized (onlineResourceWriter) {
                            onlineResourceWriter.println(resourceString);
                        }

                    }catch(WorkIgnoredException ignoredWork){
                            if(ignored.incrementAndGet() % 1000 == 0){
                                log.error("{} works ignored.", ignored.get());
                            }
                        }
                    }
                    ));
                } catch (IOException e) {
                    log.error("Failed runnable! {}",e.getMessage());
                }
            };

            Thread thread = new Thread(r);
            threads.add(thread);
            thread.start();
        }

        //wait until completed
        for (Thread thread : threads) {
            thread.join();
            log.info("Thread {} finished", thread.getName());
        }

        flushAndClosePrinters();
    }

    private void flushPrinters() {
        synchronized (workWriter) { workWriter.flush(); }
        synchronized (workTagWriter) { workTagWriter.flush(); }
        synchronized (workCoverWriter) { workCoverWriter.flush(); }
        synchronized (onlineResourceWriter) { onlineResourceWriter.flush(); }
    }


    private String toDbString(String mainTitle) {
        return mainTitle.replaceAll(",", "&#44;");
    }

    private void generateTags(File[] files) throws IOException {
        Set<MetadataTag> uniqueTags = new HashSet<>();
        parseData(files, (bean) -> uniqueTags.addAll(bean.findAllRelationalMetadata()));

        log.warn("Creating tag.csv out of unique tags");
        progress.set(1);
        errors.set(0);
        warnings.set(0);

        for (MetadataTag uniqueTag : uniqueTags) {
            tagWriter.println(progress.get()+","+toDbDate(new Date().getTime()) +","+uniqueTag.getTag()+ ","+uniqueTag.getNamespace());

            if(progress.incrementAndGet() % 500 == 0) log.info("Written {} lines to tag.csv", progress.get());
        }

        tagWriter.flush();
        tagWriter.close();
        log.warn("Found {} unique tags!",uniqueTags.size());
        log.warn("Errors: {}", errors.get());
    }
    private String toDbDate() {
        return toDbDate(new Date().getTime());
    }

    private String toDbDate(long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(time));
    }

    private class WorkIgnoredException extends Exception {
        WorkIgnoredException(String s) {
            super(s);
        }
    }

    public PrintWriter writer(String path, String header) throws FileNotFoundException {
        File file = new File(path);
        if(file.exists()) //noinspection ResultOfMethodCallIgnored
            file.delete();

        PrintWriter workWriter = new PrintWriter(new FileOutputStream(file, true));
        workWriter.println(header);
        return workWriter;
    }
}
