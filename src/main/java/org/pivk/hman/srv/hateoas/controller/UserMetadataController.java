package org.pivk.hman.srv.hateoas.controller;

import org.pivk.hman.srv.exception.ResourceNotFoundException;
import org.pivk.hman.srv.exception.ResourceOperationNotSupported;
import org.pivk.hman.srv.jpamodel.Archive;
import org.pivk.hman.srv.jpamodel.User;
import org.pivk.hman.srv.jpamodel.UserMetadata;
import org.pivk.hman.srv.repositories.ArchiveRepository;
import org.pivk.hman.srv.repositories.TagRepository;
import org.pivk.hman.srv.repositories.UserMetadataRepository;
import org.pivk.hman.srv.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @since 0.12
 */
@Controller
public class UserMetadataController {

    private final ArchiveRepository archiveRepository;
    @Autowired
    private UserMetadataRepository userMetadataRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntityLinks entityLinks;

    @Autowired
    public UserMetadataController(ArchiveRepository archiveRepository) {
        this.archiveRepository = archiveRepository;
    }

    @RequestMapping(path = "/api/archives/{id}/userMetadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getUserMetadataForArchive(
            @PathVariable("id") Long id, Authentication authentication
    ) {
        UserMetadata data = userMetadataRepository.findOneByArchiveArchiveIdAndUserUserName(id, authentication.getName());

        if(data == null) throw new ResourceNotFoundException("User metadata not found for archive "+id);

        Resource<UserMetadata> tags = new Resource<>(data);


        { //self link
            Link t = ControllerLinkBuilder.linkTo(methodOn(UserMetadataController.class)
                    .getUserMetadataForArchive(id, null))
                    .withSelfRel();
            tags.add(t);
        }

        { //archive parent link
            Link t = entityLinks.linkToSingleResource(Archive.class, id);
            tags.add(t);
        }

        return new ResponseEntity<Object>(tags, HttpStatus.OK);
    }

    @RequestMapping(path = "/api/archives/{id}/userMetadata", method = RequestMethod.PATCH)
    public HttpEntity<?> patchUserMetadataForArchive(
            @PathVariable("id") Long id, @RequestBody UserMetadata userMetadata, Authentication authentication
    ) {
        UserMetadata data = userMetadataRepository.findOneByArchiveArchiveIdAndUserUserName(id, authentication.getName());

        if(data == null) {
            data = new UserMetadata();

            Archive parent = archiveRepository.findOne(id);
            data.setArchive(parent);

            User user = userRepository.findFirstByUserNameIgnoreCase(authentication.getName());
            data.setUser(user);
        }

        data.patch(userMetadata);

        userMetadataRepository.save(data);

        return ResponseEntity.ok().build();
    }
}