package org.pivk.hman.srv.hateoas.processor;

import org.pivk.hman.srv.exception.ResourceInternalServerErrorException;
import org.pivk.hman.srv.hateoas.controller.ResourceController;
import org.pivk.hman.srv.hateoas.controller.TagController;
import org.pivk.hman.srv.hateoas.controller.UserMetadataController;
import org.pivk.hman.srv.jpamodel.Archive;
import org.pivk.hman.srv.repositories.ArchiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


/**
 * @since 0.12
 */
@Component
public class ArchiveResourceProcessor implements ResourceProcessor<Resource<Archive>> {
    @Autowired RepositoryEntityLinks links;

    @Override
    public Resource<Archive> process(Resource<Archive> resource) {
        Long archiveId = resource.getContent().getArchiveId();

        { //tags link
            Link l = linkTo(methodOn(TagController.class)
                    .getTagsForArchive(archiveId, null))
                    .withRel("tags");
            resource.add(l);
        }

        { //resources link
            Link l = linkTo(methodOn(ResourceController.class)
                    .getResourcesForArchive(archiveId, null))
                    .withRel("resources");
            resource.add(l);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Link l = linkTo(methodOn(UserMetadataController.class)
                    .getUserMetadataForArchive(archiveId, null))
                    .withRel("userMetadata");
            resource.add(l);

            return resource;
        } else {
            throw new ResourceInternalServerErrorException(
                    "Bad security configuration! Trying to create resource links without user authentication!"
            );
        }
    }
}
