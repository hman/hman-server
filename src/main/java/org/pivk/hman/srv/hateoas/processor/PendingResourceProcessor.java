package org.pivk.hman.srv.hateoas.processor;

import org.pivk.hman.srv.hateoas.controller.VoteController;
import org.pivk.hman.srv.jpamodel.PendingChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


/**
 * @since 0.12
 */
@Component
public class PendingResourceProcessor implements ResourceProcessor<Resource<PendingChange>> {
    @Autowired RepositoryEntityLinks links;

    @Override
    public Resource<PendingChange> process(Resource<PendingChange> resource) {
        Long changeId = resource.getContent().getPendingChangeId();

        { //pending link
            Link l = links.linkToSingleResource(PendingChange.class, changeId);
            Link ll = new Link(l.getHref()+"/vote").withRel("vote");
            resource.add(ll);
        }

        // add accept link (only visible to admin and moderators)
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                String role = authority.getAuthority();
                if(role.equalsIgnoreCase("role_admin")
                        || role.equalsIgnoreCase("role_moderator")) {
                    Link l = linkTo(methodOn(VoteController.class)
                            .acceptChangeRequest(changeId, null))
                            .withRel("accept");
                    resource.add(l);
                }
            }
        }

        return resource;
    }
}
