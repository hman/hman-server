package org.pivk.hman.srv.hateoas.controller;

import org.pivk.hman.srv.jpamodel.Archive;
import org.pivk.hman.srv.jpamodel.Resource;
import org.pivk.hman.srv.repositories.ArchiveRepository;
import org.pivk.hman.srv.resources.ResourcesController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @since 0.12
 */
@Controller
@RequestMapping("/api/archives")
public class ResourceController {
    private final ArchiveRepository archiveRepository;

    @Autowired
    private EntityLinks entityLinks;

    @Autowired
    public ResourceController(ArchiveRepository archiveRepository) {
        this.archiveRepository = archiveRepository;
    }

    @RequestMapping(path = "/{id}/resources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getResourcesForArchive(
            @PathVariable("id") Long id, Authentication authentication
    ) {
        Set<org.springframework.hateoas.Resource<Resource>> content = new HashSet<>();

        Archive archive = archiveRepository.findOne(id);
        for (Resource resource : archive.getResources()) {
            org.springframework.hateoas.Resource<Resource> tags = new org.springframework.hateoas.Resource(resource);
            { //resource link
                Link t = ControllerLinkBuilder.linkTo(methodOn(ResourcesController.class)
                        .viewWork(id,null, null))
                        .withRel("location");
                tags.add(t);
            }
            content.add(tags);
        }

        Resources<org.springframework.hateoas.Resource<Resource>> tags = new Resources<>(content);

        { //self link
            Link t = ControllerLinkBuilder.linkTo(methodOn(ResourceController.class).getResourcesForArchive(id,null)).withSelfRel();
            tags.add(t);
        }

        { //archive parent link
            Link t = entityLinks.linkToSingleResource(Archive.class, id);
            tags.add(t);
        }

        return new ResponseEntity<Object>(tags, HttpStatus.OK);
    }
}