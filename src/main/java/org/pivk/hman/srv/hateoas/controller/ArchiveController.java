package org.pivk.hman.srv.hateoas.controller;

import org.pivk.hman.srv.exception.ResourceOperationNotSupported;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @since 0.12
 */
@Controller
public class ArchiveController {

    @RequestMapping(path = "/api/archives/search/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getItems(
            @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
            @RequestParam(value = "size", defaultValue = "24", required = false) Integer size,
            @RequestParam(value = "sort", defaultValue = "created", required = false) String sort,
            @RequestParam(value = "filter", defaultValue = "", required = false) String filter,
            Sort sorting, Authentication authentication
    ) {
        //TODO #2 implement custom JDBC search
        throw new ResourceOperationNotSupported("Archive search not implemented yet!");
    }


//    @RequestMapping(path = "/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
//    public HttpEntity<?> patchArchive(
//            @PathVariable("id") Long id, @RequestBody Archive archive, Authentication authentication
//    ) {
//        Archive originArchive = archiveRepository.findOne(id);
//        if(originArchive == null) throw new ResourceNotFoundException("Archive "+id+" does not exist");
//
//
//    }
}