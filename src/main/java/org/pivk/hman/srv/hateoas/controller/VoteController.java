package org.pivk.hman.srv.hateoas.controller;

import org.pivk.hman.srv.exception.ResourceNotFoundException;
import org.pivk.hman.srv.exception.ResourceOperationNotSupported;
import org.pivk.hman.srv.jpamodel.PendingChange;
import org.pivk.hman.srv.jpamodel.User;
import org.pivk.hman.srv.jpamodel.Vote;
import org.pivk.hman.srv.repositories.ArchiveRepository;
import org.pivk.hman.srv.repositories.PendingChangesRepository;
import org.pivk.hman.srv.repositories.UserRepository;
import org.pivk.hman.srv.repositories.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @since 0.12
 */
@Controller
public class VoteController {

    private final ArchiveRepository archiveRepository;
    @Autowired
    PendingChangesRepository changesRepository;

    @Autowired
    VoteRepository voteRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    public VoteController(ArchiveRepository archiveRepository) {
        this.archiveRepository = archiveRepository;
    }

    @RequestMapping(path = "/api/pendingChanges/{id}/vote", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getPendingChanges(
            @PathVariable("id") Long id, Authentication authentication
    ) {
        PendingChange change = changesRepository.findOne(id);
        if(change == null) throw new ResourceNotFoundException("Change request '"+id+"' not found");

        Vote userVote = voteRepository.findFirstByUserUserNameIgnoreCaseAndVoteId(authentication.getName(), id);

        if(userVote != null)
            return new ResponseEntity<>(HttpStatus.CREATED);

        User user = userRepository.findFirstByUserNameIgnoreCase(authentication.getName());

        //create a vote
        userVote = new Vote();
        userVote.setChange(change);
        userVote.setUser(user);

        voteRepository.save(userVote);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(path = "/api/pendingChanges/{id}/accept", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> acceptChangeRequest(
            @PathVariable("id") Long id, Authentication authentication
    ) {
        PendingChange change = changesRepository.findOne(id);
        if(change == null) throw new ResourceNotFoundException("Change request '"+id+"' not found");

        //TODO #3 implement change request accept method
        throw new ResourceOperationNotSupported("Change request accept not implemented yet! Change not accepted.");
    }


}