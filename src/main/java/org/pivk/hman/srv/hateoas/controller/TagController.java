package org.pivk.hman.srv.hateoas.controller;

import org.pivk.hman.srv.jpamodel.Archive;
import org.pivk.hman.srv.jpamodel.Tag;
import org.pivk.hman.srv.repositories.ArchiveRepository;
import org.pivk.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @since 0.12
 */
@Controller
@RequestMapping("/api/archives")
public class TagController {

    private final ArchiveRepository archiveRepository;
    @Autowired
    private EntityLinks entityLinks;

    @Autowired
    public TagController(ArchiveRepository archiveRepository) {
        this.archiveRepository = archiveRepository;
    }

    @RequestMapping(path = "/{id}/tags", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getTagsForArchive(
            @PathVariable("id") Long id, Authentication authentication
    ) {
        Set<Tag> content = new HashSet<>();

        long time = TimeUtil.measureTimeMs(() -> {
            Archive archive = archiveRepository.findOne(id);
            for (Tag tag : archive.getTags()) {
                synchronized (content){
                    content.add(tag);
                }
            }
        });

        System.out.println(time);

        Resources<Tag> tags = new Resources<>(content);


        { //self link
            Link t = ControllerLinkBuilder.linkTo(methodOn(TagController.class).getTagsForArchive(id, null)).withSelfRel();
            tags.add(t);
        }

        { //archive parent link
            Link t = entityLinks.linkToSingleResource(Archive.class, id);
            tags.add(t);
        }

        return new ResponseEntity<Object>(tags, HttpStatus.OK);
    }
}