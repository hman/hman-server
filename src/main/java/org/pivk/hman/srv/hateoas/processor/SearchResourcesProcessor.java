package org.pivk.hman.srv.hateoas.processor;

import org.pivk.hman.srv.hateoas.controller.ArchiveController;
import org.springframework.data.rest.webmvc.RepositorySearchesResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @since 0.12
 */
@Component
public class SearchResourcesProcessor implements ResourceProcessor<RepositorySearchesResource> {

    @Override
    public RepositorySearchesResource process(RepositorySearchesResource repositorySearchesResource) {
        Link l = linkTo(methodOn(ArchiveController.class)
                .getItems(null,null,null,null,null,null))
                .withRel("search");
        repositorySearchesResource.add(l);

        return repositorySearchesResource;
    }
}