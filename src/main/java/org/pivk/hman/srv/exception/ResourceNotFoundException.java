package org.pivk.hman.srv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends ResourceException {

    public ResourceNotFoundException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public ResourceNotFoundException(String s) {
        super(HttpStatus.NOT_FOUND, s);
    }
}