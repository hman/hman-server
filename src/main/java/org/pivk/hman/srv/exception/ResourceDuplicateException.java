package org.pivk.hman.srv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ResourceDuplicateException extends ResourceException {

    public ResourceDuplicateException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public ResourceDuplicateException(String s) {
        super(HttpStatus.CONFLICT, s);
    }
}