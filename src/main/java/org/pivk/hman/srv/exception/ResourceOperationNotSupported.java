package org.pivk.hman.srv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
public class ResourceOperationNotSupported extends ResourceException {

    public ResourceOperationNotSupported(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public ResourceOperationNotSupported(String s) {
        super(HttpStatus.NOT_IMPLEMENTED, s);
    }
}
