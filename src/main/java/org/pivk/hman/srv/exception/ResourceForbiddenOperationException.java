package org.pivk.hman.srv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class ResourceForbiddenOperationException extends ResourceException {

    public ResourceForbiddenOperationException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

}