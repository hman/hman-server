package org.pivk.hman.srv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @since 0.11
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ResourceBadRequestException extends ResourceException {

    public ResourceBadRequestException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }

    public ResourceBadRequestException(String s) {
        super(HttpStatus.BAD_REQUEST, s);
    }
}