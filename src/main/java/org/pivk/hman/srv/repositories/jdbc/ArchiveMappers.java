package org.pivk.hman.srv.repositories.jdbc;

import com.pettt.hman.metadata.converter.util.Util;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

//TODO #2 port jdbc implementation to archive system
final class ArchiveMappers {
    @SuppressWarnings("FieldCanBeLocal")
    static class ArchiveMapper implements RowMapper<ArchivePojo> {
        private int WORK_ID = 1;
        private int GALLERY_OWNER = 2;
        private int TITLE = 3;
        private int CREATED = 4;
        private int THUMBNAIL_HASH = 5;
        private int THUMBNAIL_URL = 6;
        private int CONCAT_TAGS = 7;
        private int ONLINE_URLS = 8;
        private int OFFFLINE_RESOURCE_ID = 9;
        private int MAX_PAGES = 10;
        private int LAST_READ = 11;
        private int RATING = 12;
        private int READ_COUNT = 13;
        private int TRASH = 14;
        private int FAVORITE_GROUPS = 15;
        private int FAVORITES = 16;

        @Override
        public ArchivePojo mapRow(ResultSet rs, int rowNum) throws SQLException {
            ArchivePojo e = new ArchivePojo();
            e.setWorkId(rs.getLong(WORK_ID));
            e.setGalleryOwner(rs.getLong(GALLERY_OWNER));
            //TODO UTIL FUNCTION
            e.setTitle(Util.escapeHTMLString(rs.getString(TITLE)));
            e.setCreated(rs.getTimestamp(CREATED));
            e.setRating(rs.getFloat(RATING));
            e.setPages(rs.getInt(MAX_PAGES));
            e.setLastRead(rs.getTimestamp(LAST_READ));
            e.setReadCount(rs.getInt(READ_COUNT));
            e.setThumbnailHash(rs.getString(THUMBNAIL_HASH));
            e.setThumbnailUrl(rs.getString(THUMBNAIL_URL));
            e.setTrash(rs.getBoolean(TRASH));

            e.setFavorites(rs.getInt(FAVORITES));

            Array favorites = rs.getArray(FAVORITE_GROUPS);
            if (favorites != null) {
                String[] favoritesArray = (String[]) favorites.getArray();
                if (favoritesArray != null && favoritesArray.length != 0 && favoritesArray[0] != null) {
                    e.setFavoriteGroups(Arrays.asList(favoritesArray));
                }
            }

            Array tags = rs.getArray(CONCAT_TAGS);
            if (tags != null) {
                String[] tagsArray = (String[]) tags.getArray();
                if (tagsArray != null && tagsArray.length != 0 && tagsArray[0] != null) {
                    e.setTags(Arrays.asList(tagsArray));
                }
            }

            Array onlineUrls = rs.getArray(ONLINE_URLS);
            if (onlineUrls != null) {
                String[] onlineUrlsArray = (String[]) onlineUrls.getArray();
                if (onlineUrlsArray != null && onlineUrlsArray.length != 0 && onlineUrlsArray[0] != null) {
                    e.setOnlineUrls(Arrays.asList(onlineUrlsArray));
                }
            }

            e.setOfflineUrl(rs.getString(OFFFLINE_RESOURCE_ID));

            if (e.getThumbnailUrl() == null) {
                e.setThumbnailUrl("/resource/thumbnail/dummy-cover.png");
                e.setThumbnailHash("-1");
            }

            return e;
        }
    }

    static class WorkAnonMapper implements RowMapper<ArchivePojo> {
        int WORK_ID = 1;
        int GALLERY_OWNER = 2;
        int TITLE = 3;
        int CREATED = 4;
        int MAX_PAGES = 5;
        int THUMBNAIL_URL = 6;
        int THUMBNAIL_HASH = 7;
        int CONCAT_TAGS = 8;
        int ONLINE_URLS = 9;
        int OFFFLINE_RESOURCE_ID = 10;
        @Override
        public ArchivePojo mapRow(ResultSet rs, int rowNum) throws SQLException {
            ArchivePojo e = new ArchivePojo();
            e.setWorkId(rs.getLong(WORK_ID));
            e.setGalleryOwner(rs.getLong(GALLERY_OWNER));
            e.setTitle(Util.escapeHTMLString(rs.getString(TITLE)));
            e.setCreated(rs.getTimestamp(CREATED));
            e.setPages(rs.getInt(MAX_PAGES));
            e.setThumbnailHash(rs.getString(THUMBNAIL_HASH));
            e.setThumbnailUrl(rs.getString(THUMBNAIL_URL));

            Array tags = rs.getArray(CONCAT_TAGS);
            if(tags != null){
                String[] tagsArray = (String[]) tags.getArray();
                if(tagsArray != null && tagsArray.length != 0 && tagsArray[0] != null){
                    e.setTags(Arrays.asList(tagsArray));
                }
            }

            Array onlineUrls = rs.getArray(ONLINE_URLS);
            if(onlineUrls != null) {
                String[] onlineUrlsArray = (String[]) onlineUrls.getArray();
                if(onlineUrlsArray != null && onlineUrlsArray.length != 0 && onlineUrlsArray[0] != null){
                    e.setOnlineUrls(Arrays.asList(onlineUrlsArray));
                }
            }

            e.setOfflineUrl(rs.getString(OFFFLINE_RESOURCE_ID));

            if(e.getThumbnailUrl() == null) {
                e.setThumbnailUrl("/resource/thumbnail/dummy-cover.png");
                e.setThumbnailHash("-1");
            }

            return e;
        }
    }

    static class WorkPersonalMapper implements RowMapper<ArchivePersonalMetadataPojo> {

        @Override
        public ArchivePersonalMetadataPojo mapRow(ResultSet rs, int rowNum) throws SQLException {
            ArchivePersonalMetadataPojo e = new ArchivePersonalMetadataPojo();
            e.setWorkId(rs.getLong(1));
            e.setLastRead(rs.getTimestamp(2));
            e.setReadCount(rs.getInt(3));
            e.setRating(rs.getFloat(4));
            e.setTrash(rs.getBoolean(5));

            Array favorites = rs.getArray(6);
            if (favorites != null) {
                String[] favoritesArray = (String[]) favorites.getArray();
                if (favoritesArray != null && favoritesArray.length != 0 && favoritesArray[0] != null) {
                    e.setFavoriteGroups(Arrays.asList(favoritesArray));
                }
            }

            e.setMetadataId(rs.getLong(7));

            return e;
        }
    }
}