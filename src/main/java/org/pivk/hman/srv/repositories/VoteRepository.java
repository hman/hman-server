package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.Vote;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@SuppressWarnings("UnusedReturnValue")
@RepositoryRestResource(exported = false)
public interface VoteRepository extends Repository<Vote, Long> {
    Vote findFirstByUserUserNameIgnoreCaseAndVoteId(String username, Long voteId);

    @RestResource(exported = false)
    <S extends Vote> S save(S entity);
}
