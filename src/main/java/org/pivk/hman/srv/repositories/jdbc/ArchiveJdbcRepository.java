package org.pivk.hman.srv.repositories.jdbc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.BiConsumer;

import static org.pivk.hman.srv.repositories.jdbc.ArchiveJdbcFilters.EFilters.*;
import static org.pivk.hman.srv.repositories.jdbc.ArchiveJdbcFilters.*;
import static org.pivk.hman.srv.repositories.jdbc.ArchiveJdbcFilters.EFilters.USER_FILTER;


//TODO #2 port jdbc implementation to archive system
//custom jdbc queries
@Component @Slf4j
public class ArchiveJdbcRepository {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ArchiveJdbcRepository(JdbcTemplate jdbcTemplate) { this.jdbcTemplate = jdbcTemplate; }


    private void queryAllWorksByRequest(StringBuilder jdbcRequest, SearchRequest searchRequest,
                                        boolean orderAndLimit, BiConsumer<String,Object> request){
        List<Object> usedFilters = new ArrayList<>();
        StringBuilder filter = new StringBuilder();

        //TAG Filter
        searchTag(usedFilters, jdbcRequest, searchRequest.getTagsFilter());

        //WHERE FILTER

        {//USER FILTER
            append(usedFilters, filter, searchRequest.getUser(), USER_FILTER);
        }
        {//PAGES FILTER
            append(usedFilters, filter, searchRequest.getMinPages(), MIN_PAGES);
            append(usedFilters, filter, searchRequest.getMaxPages(), MAX_PAGES);
        }
        {//RATING FILTER
            append(usedFilters, filter, searchRequest.getMinRating(), MIN_RATING);
            append(usedFilters, filter, searchRequest.getMaxRating(), MAX_RATING);
        }
        {//READ COUNT FILTER
            append(usedFilters, filter, searchRequest.getMinRead(), MIN_READ);
            append(usedFilters, filter, searchRequest.getMaxRead(), MAX_READ);
        }
        {//LAST READ FILTER
            append(usedFilters, filter, searchRequest.getMinLastReadMinutes(), MIN_LAST_READ);
            append(usedFilters, filter, searchRequest.getMaxLastReadMinutes(), MAX_LAST_READ);
        }
        {//CREATED FILTER
            append(usedFilters, filter, searchRequest.getMinOldMinutes(), MIN_CREATED);
            append(usedFilters, filter, searchRequest.getMaxOldMinutes(), MAX_CREATED);
        }
        {//SEARCH
            append(usedFilters, filter, searchRequest.getSearch(), SEARCH);
        }
        {//FAVORITE and TRASHED
            append(usedFilters, filter, searchRequest.getFavorite(), FAVORITE);
            append(usedFilters, filter, searchRequest.getTrash(), TRASHED);

            for (String favoriteGroupFilter : searchRequest.getFavoritesFilter()) {
                append(usedFilters, filter, favoriteGroupFilter, FAVORITES_FILTER);
            }
        }

        if(filter.length() >= 4)
            jdbcRequest.append(" WHERE ").append(filter.toString().substring(4));

        if(orderAndLimit){
            jdbcRequest.append(" ORDER BY ").append("work_id").append(" ");
            jdbcRequest.append(LIMIT);
            usedFilters.add(searchRequest.getSize());
            jdbcRequest.append(OFFSET);
            usedFilters.add(searchRequest.getPage()*searchRequest.getSize());
        }


        log.info("SQL: {}", jdbcRequest.toString());
        request.accept(jdbcRequest.toString(), usedFilters.toArray());
    }

    public List<ArchivePojo> findAllWorksByRequest(SearchRequest searchRequest) {
        List<ArchivePojo> groups = new ArrayList<>();
        StringBuilder jdbcRequest;
        if(searchRequest.getUser() != null){
            jdbcRequest = new StringBuilder(SELECT_USER + FROM_USER);
        } else {
            log.error("Anonymous requests not implemented");
            throw new UnsupportedOperationException("Anon requests not implemented");
        }

        queryAllWorksByRequest(jdbcRequest, searchRequest, true, ((jdbc, array) -> {
            List<ArchivePojo> t = jdbcTemplate.query(jdbcRequest.toString(), (Object[]) array, new ArchiveMappers.ArchiveMapper());
            groups.addAll(t);
        }));

        return groups;
    }

//    public PaginationPojo countAllWorksByRequest(SearchRequest searchRequest) {
//        final PaginationPojo[] paginationPojo = new PaginationPojo[1];
//
//        StringBuilder jdbcRequest;
//        if(searchRequest.getUser() != null){
//            jdbcRequest = new StringBuilder(SELECT_COUNT + FROM_USER);
//        } else {
//            log.error("Anonymous requests not implemented");
//            throw new UnsupportedOperationException("Anon requests not implemented");
//        }
//
//        queryAllWorksByRequest(jdbcRequest, searchRequest, false, ((jdbc, array) -> {
//            int size = jdbcTemplate.queryForObject(jdbcRequest.toString(), (Object[]) array, Integer.class);
//            boolean firstPage = searchRequest.getPage() == 0;
//            int items = searchRequest.getPage() * searchRequest.getSize();
//            boolean lastPage = (items <= size) && (items >= size - searchRequest.getSize());
//
//            paginationPojo[0] = new PaginationPojo(searchRequest.getPage(),
//                    firstPage, lastPage,
//                    size/searchRequest.getSize(),
//                    searchRequest.getSize());
//        }));
//
//        return paginationPojo[0];
//    }

    //create filters on arrays
    private void searchTag(List<Object> usedFilters, StringBuilder jdbcRequest, Map<String, List<String>> tagFilter) {
        if(tagFilter.size() == 0) return;

        String sep = ",";
        StringBuilder filterSql = new StringBuilder();

        for (String category : tagFilter.keySet()) {
            for (String filters : tagFilter.getOrDefault(category, new ArrayList<>())) {
                if(category.equalsIgnoreCase("include")){
                    filterSql.append(" work_tag_concat_mv.concat_tags && ");
                } else if(category.equalsIgnoreCase("includeRaw")){
                    filterSql.append(" work_tag_concat_mv.concat_tags_raw && ");
                } else if(category.equalsIgnoreCase("exclude")){
                    filterSql.append(" NOT work_tag_concat_mv.concat_tags && ");
                } else if(category.equalsIgnoreCase("excludeRaw")){
                    filterSql.append(" NOT work_tag_concat_mv.concat_tags_raw && ");
                }

                List<String> textArray = new ArrayList<>();
                for (String filter : filters.split(sep)) {
                    textArray.add(filter.toLowerCase().trim());
                }
                filterSql.append("string_to_array(?,',') AND ");
                String commaSeparatedString = String.join(",",textArray.toArray(new String[0]));
                usedFilters.add(commaSeparatedString);
            }
        }
        if(filterSql.length() == 0) return;

        filterSql = new StringBuilder(filterSql.substring(0, filterSql.length() - 4));

        String request = FILTER_TEMPLATE.replaceAll("\\$TAGFILTERS\\$", filterSql.toString());
        jdbcRequest.append(request);
    }


    public void append(List<Object> usedFilters, StringBuilder filter, Object toAppend, EFilters filters) {
        if(toAppend == null) return;

        usedFilters.add(toAppend);
        filter.append(" AND ");
        switch (filters) {
            case MIN_PAGES:
                filter.append(MIN_PAGES_FILTER); return;
            case MAX_PAGES:
                filter.append(MAX_PAGES_FILTER); return;
            case MIN_RATING:
                filter.append(MIN_RATING_FILTER); return;
            case MAX_RATING:
                filter.append(MAX_RATING_FILTER); return;
            case MIN_READ:
                filter.append(MIN_READ_FILTER); return;
            case MAX_READ:
                filter.append(MAX_READ_FILTER); return;
            case MIN_LAST_READ:
                filter.append(MIN_LAST_READ_FILTER); return;
            case MAX_LAST_READ:
                filter.append(MAX_LAST_READ_FILTER); return;
            case MIN_CREATED:
                filter.append(MIN_CREATED_FILTER); return;
            case MAX_CREATED:
                filter.append(MAX_CREATED_FILTER); return;
            case FAVORITES_FILTER:
                filter.append(FAVORITE_GROUP_FILTER); return;
            case USER_FILTER:
                filter.append(ArchiveJdbcFilters.USER_FILTER); return;
            case SEARCH:
                filter.append("LOWER(title) LIKE ('%'|| LOWER( ? ) || '%')"); return;
            case FAVORITE:
                String fav = (String) toAppend;
                switch (fav) {
                    case "ONLY":    filter.append("favorites >= 1 "); break;
                    case "EXCLUDE": filter.append("favorites = 0 ");  break;
                    default: filter.delete(filter.length()-5,filter.length());
                        //INCLUDE: default
                }
                usedFilters.remove(usedFilters.size()-1);
                return;
            case TRASHED:
                String trash = (String) toAppend;
                switch (trash) {
                    case "ONLY":    filter.append(" (trash = true) ");  break;
                    case "EXCLUDE": filter.append(" (trash = false OR trash IS NULL) "); break;
                    default: filter.delete(filter.length()-5,filter.length());
                        //INCLUDE: default
                }
                usedFilters.remove(usedFilters.size()-1);
                return;
        }

        throw new IllegalStateException("Missing switch branch for filter SQL statement generation: "+filters+"!");
    }
}
