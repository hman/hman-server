package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.User;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface UserRepository extends Repository<User, Long> {
    User findFirstByUserNameIgnoreCase(String username);
}
