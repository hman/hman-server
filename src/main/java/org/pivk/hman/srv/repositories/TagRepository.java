package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.Tag;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface TagRepository extends Repository<Tag, Long> {

}
