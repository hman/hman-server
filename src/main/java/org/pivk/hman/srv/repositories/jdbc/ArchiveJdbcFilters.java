package org.pivk.hman.srv.repositories.jdbc;

//TODO #2 port jdbc implementation to archive system
public class ArchiveJdbcFilters {

    public static final String QUERY_HOME_LAST_READ =
            "SELECT id, container_type, last_read, username " +
                    "FROM public.home_last_read_view WHERE username=? LIMIT 12;";

    public static final String QUERY_HIGHEST_RATED_READ =
            "SELECT id, container_type, rating, username " +
                    "FROM public.home_highest_rated_view WHERE username=? LIMIT 12;";

    public static final String QUERY_HIGHEST_READ_COUNT_READ =
            "SELECT id, container_type, read_count, username " +
                    "FROM public.home_highest_read_count_view WHERE username=? LIMIT 12;";

    public static final String SELECT_ANON =
            " SELECT work_id, gallery_owner, title, created, max_pages, thumbnail_url, thumbnail_hash, concat_tags, online_urls, offline_resource_id ";

    public static final String SELECT_USER =
            "SELECT work_id, gallery_owner, title, work_metadata_view.created, thumbnail_hash, thumbnail_url, concat_tags, online_urls,  " +
                    " offline_resource_id, max_pages, last_read, rating, read_count, trash, favorite_groups,  " +
                    " CASE " +
                    "      WHEN personal_join.favorite_groups = '{}' OR personal_join.favorite_groups IS NULL THEN 0::bigint  " +
                    "      ELSE array_length(personal_join.favorite_groups, 1)  " +
                    "    END AS favorites ";

    public static final String SELECT_COUNT =
            "SELECT COUNT(work_id) ";

    public static final String FROM_ANON   = " FROM work_metadata_view ";
    public static final String FROM_USER   = " FROM work_metadata_view " +
            "LEFT JOIN (  " +
            "SELECT work_personal_metadata.work_personal_metadata_id, user_user_id, work_work_id, " +
            "    work_personal_metadata.last_read, " +
            "    work_personal_metadata.rating, " +
            "    work_personal_metadata.read_count, " +
            "    work_personal_metadata.trash, " +
            "    array_remove(array_agg(work_personal_metadata_favorite_groups.favorite_groups), NULL::character varying) AS favorite_groups " +
            "   FROM work_personal_metadata " +
            "     LEFT JOIN work_personal_metadata_favorite_groups  " +
            "     ON work_personal_metadata_favorite_groups.work_personal_metadata_work_personal_metadata_id = work_personal_metadata.work_personal_metadata_id   " +
            "  GROUP BY work_personal_metadata.work_personal_metadata_id " +
            ") AS personal_join ON work_metadata_view.work_id = personal_join.work_work_id " +
            "LEFT JOIN users ON users.user_id=user_user_id ";

    public static final String USER_FILTER = " (users.username=? OR user_user_id IS NULL) ";

    public static final String LIMIT  = " LIMIT ? ";
    public static final String OFFSET = " OFFSET ? ";

    /* pages filter */
    public static final String MIN_PAGES_FILTER       = " pages >= ? ";
    public static final String MAX_PAGES_FILTER       = " pages < ? ";
    /* rating filter */
    public static final String MIN_RATING_FILTER      = " rating >= ? ";
    public static final String MAX_RATING_FILTER      = " rating < ? ";
    /* favorite group filter */
    public static final String FAVORITE_GROUP_FILTER  = " ? = ANY(favorite_groups) ";
    /* read count filter */
    public static final String MIN_READ_FILTER        = " read_count >= ? ";
    public static final String MAX_READ_FILTER        = " read_count < ? ";
    /* last read time interval (min old, max old) */
    public static final String MIN_LAST_READ_FILTER   = " last_read < current_date - (? || ' minutes')::interval ";
    public static final String MAX_LAST_READ_FILTER   = " last_read > current_date - (? || ' minutes')::interval ";
    /* created time interval (min old, max old) */
    public static final String MIN_CREATED_FILTER     = " created < current_date - (? || ' minutes')::interval ";
    public static final String MAX_CREATED_FILTER     = " created > current_date - (? || ' minutes')::interval ";
    /* join template */
    public static final String FILTER_TEMPLATE =
            " JOIN " +
                    "  (SELECT DISTINCT(work_id) AS filtered_id " +
                    "   FROM work_tag_concat_mv " +
                    "   WHERE $TAGFILTERS$ " +
                    "   ORDER BY work_id " +
                    "   ) AS filtered ON filtered.filtered_id=work_id ";

    
    public static final String queryForPersonalMetadata = 
            "SELECT work_id, last_read, read_count, rating, trash, favorite_groups,work_personal_metadata_id " +
                    "FROM work " +
                    "JOIN " +
                    "(SELECT work_personal_metadata.work_personal_metadata_id, user_user_id, work_work_id, " +
                    "    work_personal_metadata.last_read, " +
                    "    work_personal_metadata.rating, " +
                    "    work_personal_metadata.read_count, " +
                    "    work_personal_metadata.trash, " +
                    "    array_remove(array_agg(work_personal_metadata_favorite_groups.favorite_groups), NULL::character varying) AS favorite_groups " +
                    "    FROM work_personal_metadata " +
                    "     LEFT JOIN work_personal_metadata_favorite_groups  " +
                    "     ON work_personal_metadata_favorite_groups.work_personal_metadata_work_personal_metadata_id = work_personal_metadata.work_personal_metadata_id   " +
                    "   GROUP BY work_personal_metadata.work_personal_metadata_id " +
                    ") AS personal_join ON work.work_id = personal_join.work_work_id " +
                    "LEFT JOIN users ON users.user_id=user_user_id " +
                    "WHERE (users.username=?)\n" +
                    "AND work_id=?";

    static final String updatePersonalMetada =
            "UPDATE public.work_personal_metadata " +
                    "SET last_read=?, rating=?, read_count=?, trash=?, user_user_id=?, work_work_id=? " +
                    "WHERE work_personal_metadata_id=?; ";

    static final String queryForPersonalMetadataId =
            "SELECT work_personal_metadata_id " +
                    "FROM public.work_personal_metadata " +
                    "ORDER BY work_personal_metadata_id DESC " +
                    "LIMIT 1";

    static final String createPersonalMetadata =
            "INSERT INTO public.work_personal_metadata " +
                    "(work_personal_metadata_id, last_read, rating, read_count, trash, user_user_id, work_work_id) " +
                    "VALUES(?, ?, ?, ?, ?,?,?);";
    
    public enum EFilters {
        MIN_PAGES, MAX_PAGES, MIN_RATING, MAX_RATING, MIN_READ, MAX_READ, MIN_LAST_READ, MAX_LAST_READ,
        MIN_CREATED, MAX_CREATED, SEARCH, FAVORITE, TRASHED, FAVORITES_FILTER, USER_FILTER
    }
}
