package org.pivk.hman.srv.repositories.jdbc;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

//TODO #2 port jdbc implementation to archive system
@Getter @Setter
public class ArchivePersonalMetadataPojo {
    Long metadataId;
    Long workId;
    String username;

    /* personal metadata, may be null if not exists */

    Float rating;
    Timestamp lastRead;
    Integer readCount;
    Boolean trash;
    List<String> favoriteGroups;
}
