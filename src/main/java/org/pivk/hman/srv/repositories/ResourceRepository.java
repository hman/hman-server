package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.Resource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface ResourceRepository extends CrudRepository<Resource, Long> {

}
