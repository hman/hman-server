package org.pivk.hman.srv.repositories.jdbc;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO #2 port jdbc implementation to archive system
@Data
public class SearchRequest {
    int page = 0;
    int size = 25;

    String user;

    Integer minOldMinutes;
    Integer maxOldMinutes;

    Integer minLastReadMinutes;
    Integer maxLastReadMinutes;

    Integer minPages;
    Integer maxPages;

    BigDecimal minRating;
    BigDecimal maxRating;

    Integer minRead;
    Integer maxRead;

    String search;

    String favorite; //ONLY, EXCLUDE, INCLUDE (default: INCLUDE)
    String trash = "EXCLUDE";  //ONLY, EXCLUDE, INCLUDE (default: EXCLUDE)
    String hasOfflineResource; //ONLY, EXCLUDE, INCLUDE (default: INCLUDE)

    String[] favoritesFilter = new String[0];

    Map<String,List<String>> tagsFilter = new HashMap<>();

    String sort = "archiveId";

    public SearchRequest() {
        tagsFilter.put(INCLUDE_RAW, new ArrayList<>());
        tagsFilter.put(INCLUDE, new ArrayList<>());
        tagsFilter.put(EXCLUDE_RAW, new ArrayList<>());
        tagsFilter.put(EXCLUDE, new ArrayList<>());
    }

    public void excludeRawTags(String concatenatedTags) {
        tagsFilter.get(EXCLUDE_RAW).add(concatenatedTags);
    }

    public void excludeTags(String concatenatedTags) {
        tagsFilter.get(EXCLUDE).add(concatenatedTags);
    }

    public void includeRawTags(String concatenatedTags) {
        tagsFilter.get(INCLUDE_RAW).add(concatenatedTags);
    }

    public void includeTags(String concatenatedTags) {
        tagsFilter.get(INCLUDE).add(concatenatedTags);
    }

    private static final String INCLUDE_RAW = "includeRaw";
    private static final String INCLUDE = "include";
    private static final String EXCLUDE_RAW = "excludeRaw";
    private static final String EXCLUDE = "exclude";
}
