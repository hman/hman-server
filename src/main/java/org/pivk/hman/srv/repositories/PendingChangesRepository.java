package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.PendingChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource
public interface PendingChangesRepository extends JpaRepository<PendingChange, Long> {
    /*
        Disable hateoas POST, PUT, PATCH export (Voting System!)
     */
    @RestResource(exported = false) @Override
    <S extends PendingChange> List<S> save(Iterable<S> entities);
    @RestResource(exported = false) @Override
    <S extends PendingChange> S save(S entity);
    @RestResource(exported = false) @Override
    <S extends PendingChange> S saveAndFlush(S entity);
    @RestResource(exported = false) @Override
    void delete(Long aLong);
    @RestResource(exported = false) @Override
    void delete(Iterable<? extends PendingChange> entities);
    @RestResource(exported = false) @Override
    void deleteAll();
    @RestResource(exported = false) @Override
    void delete(PendingChange entity);
    @RestResource(exported = false) @Override
    void deleteAllInBatch();
    @RestResource(exported = false) @Override
    void deleteInBatch(Iterable<PendingChange> entities);
}
