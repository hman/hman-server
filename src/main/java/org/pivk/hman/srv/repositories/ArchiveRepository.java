package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.Archive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource
public interface ArchiveRepository extends JpaRepository<Archive, Long>
{
    //needed so that /search is generated
    @Query("select u from Archive u")
    Page<Archive> findAll(Pageable page);

    /*
        Disable hateoas POST, PUT, PATCH export (Voting System!)
     */
    @RestResource(exported = false) @Override
    <S extends Archive> List<S> save(Iterable<S> entities);
    @RestResource(exported = false) @Override
    <S extends Archive> S save(S entity);
    @RestResource(exported = false) @Override
    <S extends Archive> S saveAndFlush(S entity);
    @RestResource(exported = false) @Override
    void delete(Long aLong);
    @RestResource(exported = false) @Override
    void delete(Iterable<? extends Archive> entities);
    @RestResource(exported = false) @Override
    void deleteAll();
    @RestResource(exported = false) @Override
    void delete(Archive entity);
    @RestResource(exported = false) @Override
    void deleteAllInBatch();
    @RestResource(exported = false) @Override
    void deleteInBatch(Iterable<Archive> entities);
}
