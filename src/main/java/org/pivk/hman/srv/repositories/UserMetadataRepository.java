package org.pivk.hman.srv.repositories;

import org.pivk.hman.srv.jpamodel.UserMetadata;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface UserMetadataRepository extends CrudRepository<UserMetadata, Long> {
    UserMetadata findOneByArchiveArchiveIdAndUserUserName(Long archiveId, String userName);
}
