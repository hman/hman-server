package org.pivk.hman.srv.repositories.jdbc;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unused") //library class
//TODO #2 port jdbc implementation to archive system
@Getter @Setter
public class ArchivePojo {
    Long workId;
    Long galleryOwner;

    String title;

    Timestamp created;
    Integer pages;

    String thumbnailHash;
    String thumbnailUrl;
    List<String> tags;
    List<String> onlineUrls;
    String offlineUrl;

    /* personal metadata, may be null if not exists */

    Float rating;
    Timestamp lastRead;
    Integer readCount;
    Boolean trash;
    Integer favorites;
    List<String> favoriteGroups;
}