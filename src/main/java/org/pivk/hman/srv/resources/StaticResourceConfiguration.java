package org.pivk.hman.srv.resources;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * TODO #4 implement and replace FixedStaticResourceConfiguration
 * @see FixedStaticResourceConfiguration
 */
@Configuration @Slf4j
public class StaticResourceConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.warn("Configurable static resource handler not implemented");
    }
}