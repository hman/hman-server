package org.pivk.hman.srv.resources;

import lombok.extern.slf4j.Slf4j;
import org.pivk.hman.srv.resources.StaticResourceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Configures static resources to be served at the static location
 * <pre>
 *     /srv/hman/resource/
 * </pre>
 *
 * Will get replaced by {@link StaticResourceConfiguration} on 1.0 release where the path can be configured via property.
 *
 * @see StaticResourceConfiguration
 * @since 0.9
 */
@Configuration @Slf4j @Deprecated
public class FixedStaticResourceConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        String RESOURCE_PATH = "/srv/hman/resource/";
//        //warn user that the resource path is currently static
//        log.warn("Using default resource path {}", RESOURCE_PATH);
//        registry.addResourceHandler("/resource/**").addResourceLocations("file:" + RESOURCE_PATH);

//        registry
//                .addResourceHandler("/resource/**")
//                .addResourceLocations("/resources/"); //located in webapp!
    }

}