package org.pivk.hman.srv.resources;

import org.pivk.hman.srv.exception.ResourceOperationNotSupported;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/api/resources")
public class ResourcesController {

    @GetMapping(value = "/{resourceId}")
    public ModelAndView viewWork(@PathVariable("resourceId") Long resourceId, Model model, Authentication authentication) {
        //TODO #4 implement resource resolver
        //redirect on view
        //download on archive
        throw new ResourceOperationNotSupported("Resource resolving not implemented yet");
    }
}
