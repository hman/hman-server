//package org.pivk.hman.srv.webupload;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.pivk.hman.srv.rest.works.repository.WorkRepository;
//import org.pivk.hman.srv.jpamodel.collection.CollectionCover;
//import org.pivk.hman.srv.jpamodel.collection.Collection;
//import org.pivk.hman.srv.jpamodel.collection.CollectionDetails;
//import org.pivk.hman.srv.rest.collections.repository.CollectionRepository;
//import org.pivk.hman.srv.jpamodel.gallery.Gallery;
//import org.pivk.hman.srv.jpamodel.gallery.GalleryCover;
//import org.pivk.hman.srv.jpamodel.gallery.GalleryDetails;
//import org.pivk.hman.srv.rest.galleries.repository.GalleryRepository;
//import org.pivk.hman.srv.jpamodel.work.Tag;
//import org.pivk.hman.srv.jpamodel.work.*;
//import org.pivk.hman.srv.webupload.UploadSpecification.GallerySpecification;
//import org.pivk.hman.srv.webupload.UploadSpecification.WorkSpecification;
//import org.pivk.util.FileIO;
//import org.pivk.util.ImageUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.io.*;
//import java.nio.file.Files;
//import java.sql.Timestamp;
//import java.util.*;
//
//import static org.pivk.hman.srv.webupload.UploadValidator.validateJsonMetadata;
//import static org.pivk.util.FileIO.isFile;
//
//@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
//@Service
//@Slf4j
//public class DatabaseService {
//
//    @Autowired
//    CollectionRepository collectionRepository;
//
//    @Autowired
//    GalleryRepository galleryRepository;
//
//    @Autowired
//    WorkRepository workRepository;
//
////    @Autowired
////    GalleryCoverRepository galleryCoverRepository;
//
//
//    private ObjectMapper objectMapper = new ObjectMapper();
//
//    private String SERVER_RESOURCES = "/srv/hman/resource/";
//    private Map<File, String> queuedFiles = new HashMap<>();
//
//
//    @Transactional
//    public void validateAndInsert(File file) throws IOException {
//        log.warn("----------------------------------------------");
//        log.warn("Starting upload transaction");
//
//        log.info("Validating json metadata file");
//        File map = validateJsonMetadata(file);
//
//        log.info("Parsing json metadata");
//        UploadSpecification spec = objectMapper.readValue(FileIO.readBody(new FileInputStream(map)), UploadSpecification.class);
//
//        log.info("Validating necessary gallery and work contents");
//        UploadValidator.validateGalleryFolders(file, spec);
//
//        log.info("Propagate tags and online urls down");
//        for (String release : spec.getGalleries().keySet()) {
//            GallerySpecification gallerySpec = spec.getGalleries().get(release);
//
//            gallerySpec.getTags().addAll(spec.getTags());
//            gallerySpec.getOnlineUrls().addAll(spec.getOnlineUrls());
//
//            for (String work : gallerySpec.getWorks().keySet()) {
//                WorkSpecification workSpec = gallerySpec.getWorks().get(work);
//
//                workSpec.getTags().addAll(gallerySpec.getTags());
//                workSpec.getOnlineUrls().addAll(gallerySpec.getOnlineUrls());
//            }
//        }
//
//        log.info("------- Inserting collection");
//        Long collectionId = insertCollection(spec, file);
//        log.info("------- Finished inserting collection");
//
////        log.info("------- Inserting galleries");
////        Map<Long, Long> generatedGalleryIds = new HashMap<>();
////        for (String release : spec.getGalleries().keySet()) {
////            insertGallery(spec.getGalleries().get(release),
////                    new File(file.getAbsolutePath() + "/gallery"+release), collectionId, Long.valueOf(release), generatedGalleryIds);
////        }
////        log.info("------- Finished inserting galleries");
////
////        log.info("------- Inserting works");
////        Map<String, Map<String, Long>> generatedWorkIds = new HashMap<>();
////        for (String release : spec.getGalleries().keySet()) {
////            for (String chapter : spec.getGalleries().get(release).getWorks().keySet()) {
////                insertWork(spec.getGalleries().get(release).getWorks().get(chapter), release, chapter,
////                        new File(file.getAbsolutePath() + "/gallery"+release+"/work"+chapter),
////                        generatedWorkIds, generatedGalleryIds);
////
////            }
////        }
////        log.info("------- Finished inserting works");
////
////
////        log.info("------- Updating tag - work relation");
////        for (String release : spec.getGalleries().keySet()) {
////            for (String chapter : spec.getGalleries().get(release).getWorks().keySet()) {
////                insertTags(spec.getGalleries().get(release).getWorks().get(chapter), generatedWorkIds, release, chapter);
////            }
////        }
////        log.info("------- Finished tag updates");
//
//        log.info("------- Start moving files to resources");
//        moveQueuedFiles();
//        log.info("------- Finished moving files to resources");
//
//        log.warn("Finished upload transaction");
//        log.warn("----------------------------------------------");
//    }
//
//
//    private void insertTags(WorkSpecification workSpecification,
//                            Map<String, Map<String, Long>> generatedWorkIds, String release, String chapter) {
//        for (String tag : workSpecification.getTags()) {
//            Long workId = generatedWorkIds.get(release).get(chapter);
//            Work work = workRepository.findOne(workId);
//
//            String namespace = tag.split(":")[0];
//            String frequency = null;
//            if(namespace.contains("(") && namespace.contains(")")) {
//                //TODO implement frequency
//                frequency = namespace.substring(1,namespace.indexOf(")"));
//                namespace = namespace.substring(namespace.indexOf(")")+1);
//            }
//
//
//            String name = tag.split(":")[1];
//            Tag t = tagRepository.findFirstByNamespaceAndName(namespace, name);
//            if(t == null) {
//                t = new Tag(name, namespace);
//                t.setCreated(new Date());
//            }
//
//            List<Tag> tags = work.getTags();
//            if(tags == null) {
//                tags =  new ArrayList<>();
//                work.setTags(tags);
//            }
//            tags.add(t);
//            t.getWorks().add(work);
//
//            tagRepository.save(t);
//            workRepository.save(work);
//
//            log.info("Added {} to work {}", tag, workId);
//        }
//    }
//
//    private void insertWork(WorkSpecification workSpecification,
//                            String gallery, String chapter, File root,
//                            Map<String, Map<String, Long>> generatedWorkIds, Map<Long, Long> generatedGalleryIds) throws IOException {
//        log.info("Generating work {}", root);
//        Work work = new Work();
//        work.setChapter(Integer.valueOf(chapter));
////        work.setGallery(generatedGalleryIds.get(Long.valueOf(gallery)));
//        work.setTitle(workSpecification.getTitle());
////        work.setCreated(new Date());
//
//        File coverFile = isFile(new File(root.getAbsolutePath() + "/cover.jpg"));
//
//        {//cover
//            log.info("Generating work cover");
//            WorkCover cover = new WorkCover();
//
//            cover.setWork(work);
//            cover.setCreated(new Date());
//
//            work.setCover(cover);
//
//            String hash = ImageUtil.calculateHash(new FileInputStream(coverFile));
//            cover.setBinaryHash(hash);
//        }
//
//        {//details
//            WorkDetails details = new WorkDetails();
//            details.setWork(work);
//            work.setDetails(details);
//        }
//
//        { //online urls
//            List<OnlineResource> resources = new ArrayList<>();
//            for (UploadSpecification.OnlineUrl onlineUrl : workSpecification.getOnlineUrls()) {
//                OnlineResource onlineResource = new OnlineResource();
//                onlineResource.setCreated(new Date());
//                onlineResource.setUrl(onlineUrl.getUrl());
//                onlineResource.setOrigin(onlineUrl.getOrigin());
//                onlineResource.setPages(onlineUrl.getPages());
//                onlineResource.setWork(work);
//                resources.add(onlineResource);
//            }
//
//            work.setOnlineResources(resources);
//        }
//
//        log.info("Saving work and cover, details relations");
//        workRepository.save(work);
//
//        Long id = work.getWorkId();
//        log.info("Using id {} to generate path for thumbnail", id);
//
//        {//fetch cover again
//            WorkCover cover = workCoverRepository.findOne(id);
//            log.info("Found cover again: {}", cover.getWorkCoverId());
//
//            String path = getCoverPath("work",id);
//            cover.setUrl("/resource"+path);
//            log.info("Url set: {}   Saving cover again", path);
//            workCoverRepository.save(cover);
//
//            //queue for move
//            queuedFiles.put(isFile(coverFile), SERVER_RESOURCES+path);
//        }
//
//        Map<String, Long> galleryMap = generatedWorkIds.getOrDefault(gallery, new HashMap<>());
//        galleryMap.put(chapter, id);
//        generatedWorkIds.put(gallery, galleryMap);
//
//        queuedFiles.put(root,SERVER_RESOURCES+"work/"+(work.getWorkId() % 1000)+"/");
//    }
//
//    private void insertGallery(GallerySpecification gallerySpecification, File root, Long collectionId, Long release, Map<Long, Long> generatedGalleryIds) throws IOException {
//        log.info("Generating gallery {}", root);
//        Gallery gallery = new Gallery();
////        gallery.setCollection(Math.toIntExact(collectionId));
//        gallery.setRelease(Math.toIntExact(release));
//        gallery.setTitle(gallerySpecification.getTitle());
//        gallery.setCreated(new Timestamp(new Date().getTime()));
//
//        File coverFile = isFile(new File(root.getAbsolutePath() + "/cover.jpg"));
//
//        {//cover
//            log.info("Generating gallery cover");
//            GalleryCover cover = new GalleryCover();
//
//            cover.setGallery(gallery);
//            cover.setCreated(new Date());
//
//            gallery.setCover(cover);
//
//            String hash = ImageUtil.calculateHash(new FileInputStream(coverFile));
//            cover.setBinaryHash(hash);
//        }
//
//        {//details
//            GalleryDetails details = new GalleryDetails();
//            details.setGallery(gallery);
//            gallery.setDetails(details);
//        }
//
//        {//collection relation
//
//
//        }
//
//        log.info("Saving gallery and cover, details relations");
//        galleryRepository.save(gallery);
//
//        Long id = gallery.getGalleryId();
//        log.info("Using id  {} to generate path for thumbnail", id);
//
//        {//fetch cover again
////            GalleryCover cover = galleryCoverRepository.findOne(id);
////            log.info("Found cover again: {}", cover.getGalleryCoverId());
////
////            String path = getCoverPath("gallery",id);
////            cover.setUrl("/resource"+path);
////            log.info("Url set: {}   Saving cover again", path);
////            galleryCoverRepository.save(cover);
//
//            //queue for move
////            queuedFiles.put(isFile(coverFile), SERVER_RESOURCES+path);
//        }
//
//        generatedGalleryIds.put(release, id);
//    }
//
//    private Long insertCollection(UploadSpecification spec, File root) throws IOException {
//        log.info("Generating collection {}", spec.title);
//        Collection collection = new Collection();
//        collection.setTitle(spec.getTitle());
////        collection.setCreated(new Date());
//
//        File coverFile = isFile(new File(root.getAbsolutePath() + "/cover.jpg"));
//
//        {//cover
//            log.info("Generating collection cover");
//            CollectionCover cover = new CollectionCover();
//
//            cover.setCreated(new Date());
//
//            collection.setCover(cover);
//            cover.setCollection(collection);
//
//            String hash = ImageUtil.calculateHash(new FileInputStream(coverFile));
//            cover.setBinaryHash(hash);
//        }
//
//        {//details
//            CollectionDetails details = new CollectionDetails();
//            details.setCollection(collection);
//            collection.setDetails(details);
//        }
//
//        log.info("Saving collection and cover, details relations");
//        collectionRepository.save(collection);
//
//        Long id = collection.getCollectionId();
//        log.info("Using id  {} to generate path for thumbnail", id);
//
//        {//fetch cover again
//            Collection col = collectionRepository.findFirstByCoverCollectionCoverId(id);
//            CollectionCover cover = col.getCover();
//            log.info("Found cover again: {}", cover.getCollectionCoverId());
//
//            String path = getCoverPath("collection",id);
//            cover.setUrl("/resource"+path);
//            log.info("Url set: {}   Saving cover again", path);
//            collectionRepository.save(col);
//
//            //queue for move
//            queuedFiles.put(isFile(coverFile), SERVER_RESOURCES+path);
//        }
//
//        return id;
//    }
//
//    private void moveQueuedFiles() throws IOException {
//        for (File file : queuedFiles.keySet()) {
//            String destination = queuedFiles.get(file);
//            File f = new File(destination);
//            Files.createDirectories(f.getParentFile().toPath());
//            log.warn("Copying resource: {}  to   {}", file, queuedFiles.get(file));
//            copy(file, f);
//        }
//    }
//
//    public void copy(File sourceLocation, File targetLocation) throws IOException {
//        if (sourceLocation.isDirectory()) {
//            copyDirectory(sourceLocation, targetLocation);
//        } else {
//            copyFile(sourceLocation, targetLocation);
//        }
//    }
//
//    private void copyDirectory(File source, File target) throws IOException {
//        if (!target.exists()) {
//            target.mkdir();
//        }
//
//        for (String f : source.list()) {
//            copy(new File(source, f), new File(target, f));
//        }
//    }
//
//    private void copyFile(File source, File target) throws IOException {
//        try (
//                InputStream in = new FileInputStream(source);
//                OutputStream out = new FileOutputStream(target)
//        ) {
//            byte[] buf = new byte[1024];
//            int length;
//            while ((length = in.read(buf)) > 0) {
//                out.write(buf, 0, length);
//            }
//        }
//    }
//
//    private String getCoverPath(String type, Long reservedId) {
//        String prefix = ""+(reservedId%10000);
//
//        String thumbPath = "/thumbnail/"+type+"/"+prefix+"/"+reservedId+".jpg";
//
//        return thumbPath;
//    }
//}
