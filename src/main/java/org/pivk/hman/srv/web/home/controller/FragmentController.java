package org.pivk.hman.srv.web.home.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for html fragments for works, galleries, and collections.
 * Each fragment loads its content via ajax calls.
 * <p>
 *     Fragments are located at /html/fragment/...
 *
 * @since 0.11
 */
@SuppressWarnings("Duplicates")
@Controller
public class FragmentController {
    /**
     * Returns the html fragment for a work container.
     * It populates itself via ajax calls to the REST api.
     *
     * @since 0.11
     */
    @RequestMapping(value = "/view/fragment/work/{id}/{suffix}", method = RequestMethod.GET)
    public String getWorkContainerFragment(
            @PathVariable(name = "id") Long workId,
            @PathVariable(name = "suffix") String suffix,
            Model model, Authentication authentication) {
        model.addAttribute("id",workId);
        model.addAttribute("type","work");
        model.addAttribute("key","work-"+workId);
        model.addAttribute("suffix",suffix);
        model.addAttribute("username",authentication.getName());

        return "fragments/gallery/work :: work-container-fragment";
    }

    /**
     * @since 0.11
     */
    @RequestMapping(value = "/view/fragment/gallery/{id}/{suffix}", method = RequestMethod.GET)
    public String getGalleryContainerFragment(
            @PathVariable(name = "id") Long galleryId,
            @PathVariable(name = "suffix") String suffix,
            Model model, Authentication authentication) {
        model.addAttribute("id",galleryId);
        model.addAttribute("type","gallery");
        model.addAttribute("key","gallery-"+galleryId);
        model.addAttribute("suffix",suffix);
        model.addAttribute("username",authentication.getName());

        return "fragments/gallery/gallery :: gallery-container-fragment";
    }

    /**
     * @since 0.11
     */
    @RequestMapping(value = "/view/fragment/collection/{id}/{suffix}", method = RequestMethod.GET)
    public String getCollectionContainerFragment(
            @PathVariable(name = "id") Long collectionId,
            @PathVariable(name = "suffix") String suffix,
            Model model, Authentication authentication) {
        model.addAttribute("id",collectionId);
        model.addAttribute("type","collection");
        model.addAttribute("key","collection-"+collectionId);
        model.addAttribute("suffix", suffix);
        model.addAttribute("username",authentication.getName());

        return "fragments/gallery/collection :: collection-container-fragment";
    }
}
