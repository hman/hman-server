package org.pivk.hman.srv.web.upload;

import lombok.experimental.UtilityClass;
import org.pivk.util.FileIO;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.pivk.util.FileIO.isFile;
import static org.pivk.util.FileIO.isFolder;

@UtilityClass
public class UploadValidator {
    public static File validateJsonMetadata(File root) throws IOException {
        File metadata = isFile(new File(root.getAbsolutePath() + "/metadata.json"));
        isFile(new File(root.getAbsolutePath() + "/cover.jpg"));
        isFile(new File(root.getAbsolutePath() + "/back.jpg"));

        return metadata;
    }

    public static void validateGalleryFolders(File root, UploadSpecification spec) throws IOException {
        for (String id : spec.getGalleries().keySet()) {
            File gallery = isFolder(root.getAbsolutePath()+"/gallery"+id);
            isFile(new File(root.getAbsolutePath() + "/cover.jpg"));
            isFile(new File(root.getAbsolutePath() + "/back.jpg"));

            validateWorkFolders(gallery, spec.getGalleries().get(id));
        }
    }

    private static void validateWorkFolders(File gallery, UploadSpecification.GallerySpecification gallerySpecification) throws IOException {
        for (String id : gallerySpecification.getWorks().keySet()) {
            isFolder(gallery.getAbsolutePath()+"/work"+id);
            isFile(new File(gallery.getAbsolutePath() + "/cover.jpg"));
            isFile(new File(gallery.getAbsolutePath() + "/back.jpg"));
        }

    }
}
