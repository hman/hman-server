package org.pivk.hman.srv.web.interfaces;

import java.util.List;

public interface OnlineUrlSpecification {
    String getUrl();
    Integer getPages();
    String getOrigin();
    List<String> getType();
}