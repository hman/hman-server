package org.pivk.hman.srv.web.home.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.pivk.util.FileIO.isFolder;

/**
 * @since 0.11
 */
@SuppressWarnings("Duplicates")
@Controller @Slf4j
public class HomeController {
//    @Autowired
//    WorkRepository workRepository;
//    @Autowired
//    UserRepository userRepository;
//    @Autowired
//    WorkService workService;

    @GetMapping("/")
    public String homeDefault(){
        return "redirect:/home";
    }



    @GetMapping("/home")
    public String home(Model model, Authentication authentication) {
        if(authentication == null) {
            return "/home";
        }

//        model.addAttribute("newworks",homeViewService.findTop12NewWorks());
//        model.addAttribute("newgalleries",homeViewService.findTop12NewGalleries());
//        model.addAttribute("newcollections",homeViewService.findTop12NewCollections());
//        model.addAttribute("beans",homeViewService.findTop12LastReadWorks(authentication.getName()));
//        model.addAttribute("beans2",homeViewService.findTop12HighestRatedWorks(authentication.getName()));
//        model.addAttribute("beans3",homeViewService.findTop12HighestReadCountWorks(authentication.getName()));

//        SearchRequest request = new SearchRequest();
//        String searchInput = parseSearchRequest(request, query);
//
//        request.setPage((page == null)?0:page);
//
//        request.setUser(authentication.getName());
//
//        request.setSort(convertSort(sort));
//
//        request.setSize(48);
//
//        List<WorkPojo> works = repository.findAllWorksByRequest(request);
//        PaginationPojo pagination = repository.countAllWorksByRequest(request);
//
//        model.addAttribute("works", works);
//        model.addAttribute("galleryPage", pagination);
//        model.addAttribute("query", query);
//        model.addAttribute("searchRequest",searchInput);
//        model.addAttribute("sort", sort);


        return "/home";
    }

    @GetMapping(value = "/view/work/{workId}")
    public String viewWork(@PathVariable("workId") Long workId, Model model, Authentication authentication) throws IOException {

        RedirectView redirectView = new RedirectView();
        log.warn("Viewing work with id {}",workId);

        log.warn("Checking if there are offline resources");
//        WorkPojo work = repository.findOne(workId);
//        if(work == null){
//            redirectView.setUrl("/403");
//            return "/403";
//        }

        //update read count
//        workService.updatePersonalMetadata(
//                workId,
//                userRepository.findFirstByUserName(authentication.getName()).getUserId(),
//                e -> {
//                    e.setReadCount(e.getReadCount() + 1);
////                    e.setLastRead(new Date());
//                }
//        );

//        log.warn("Offline available? {}", work.getOfflineUrl() != null);

        model.addAttribute("workid", workId);

        File upload = isFolder("/srv/hman/resource/work/"+(workId%1000));

        List<String> fileNames = new ArrayList<>();
        for (File file : upload.listFiles()) {
            if(!(file.getName().equalsIgnoreCase("cover.jpg") ||
                    file.getName().equalsIgnoreCase("back.jpg"))){
                fileNames.add(file.getName());
            }
        }

//        fileNames.sort(new AlphanumComparator());

        model.addAttribute("files",fileNames);
        model.addAttribute("pages",fileNames.size());
        model.addAttribute("base", "/resource/work/"+(workId%1000)+"/");
        model.addAttribute("page",0);

        return "viewer";
    }

    @GetMapping(value = "/view/gallery/{galleryId}")
    public String viewGallery(@PathVariable("galleryId") Long galleryId, Model model, Authentication authentication) throws IOException {

        log.warn("Viewing gallery with id {}",galleryId);

        log.warn("Checking if there are offline resources");
//        GalleryPojo gallery = galleryRepository.findOne(galleryId);
//        if(gallery == null){
//            return "/40x?status=404";
//        }

        //update read count
        // TODO gallery read count
//        workService.updatePersonalMetadata(
//                galleryId,
//                userRepository.findFirstByUserName(authentication.getName()).getUserId(),
//                e -> {
//                    e.setReadCount(e.getReadCount() + 1);
//                    e.setLastRead(new Date());
//                }
//        );

        model.addAttribute("galleryid", galleryId);

        Map<Long, List<String>> fileNameMap = new HashMap<>();
        Map<Long, Integer> pagesMap = new HashMap<>();
        Map<Long, String> baseMap = new HashMap<>();
        Map<Integer, Long> chapterMap = new HashMap<>();
        List<Integer> sortedChapters = new ArrayList<>();


        Long currentWork = -1L;
        Integer currentChapter = Integer.MAX_VALUE;
//        for (Long id : gallery.getWorks()) {
//            Work work = workRepository.findOne(id);
//            if(work.getChapter() < currentChapter) {
//                currentChapter = work.getChapter();
//                currentWork = work.getWorkId();
//            }
//            chapterMap.put(work.getChapter(),work.getWorkId());
//            sortedChapters.add(work.getChapter());
//        }

        sortedChapters.sort(Comparator.naturalOrder());

        model.addAttribute("currentwork",currentWork);
        model.addAttribute("currentchapter",currentChapter);
        model.addAttribute("sortedchapters",sortedChapters);

//        for (Long workId : gallery.getWorks()) {
//            File upload = isFolder("/srv/hman/resource/work/"+(workId%1000));
//
//            List<String> fileNames = new ArrayList<>();
//            for (File file : upload.listFiles()) {
//                if(!(file.getName().equalsIgnoreCase("cover.jpg") ||
//                        file.getName().equalsIgnoreCase("back.jpg"))){
//                    fileNames.add(file.getName());
//                }
//            }
//
//            fileNames.sort(new AlphanumComparator());
//
//            fileNameMap.put(workId,fileNames);
//            pagesMap.put(workId,fileNames.size());
//            baseMap.put(workId, "/resource/work/"+(workId%1000)+"/");
//        }

        model.addAttribute("filenamemap",fileNameMap);
        model.addAttribute("pagesmap",pagesMap);
        model.addAttribute("basemap",baseMap);
        model.addAttribute("chaptermap",chapterMap);

        return "galleryviewer";
    }

//    private String parseSearchRequest(SearchRequest request, String query) throws SearchFormatException {
//        if(query == null || query.isEmpty()) return "";
//
//        return SearchParser.parseSearch(query, request);
//    }

    @GetMapping("/40x")
    public String error() {
        return "/error/40x";
    }
}
