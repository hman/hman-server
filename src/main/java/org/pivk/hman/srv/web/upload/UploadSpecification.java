package org.pivk.hman.srv.web.upload;

import lombok.Data;

import java.util.*;

@Data
public class UploadSpecification {
    String title = "unknown";
    Set<String> tags = new HashSet<>();
    List<OnlineUrl> onlineUrls = new ArrayList<>();
    Map<String,GallerySpecification> galleries = new HashMap<>();


    @Data
    static class GallerySpecification {
        String title = "unknown";
        Set<String> tags = new HashSet<>();
        List<OnlineUrl> onlineUrls = new ArrayList<>();
        Map<String, WorkSpecification> works = new HashMap<>();
    }

    @Data
    static class WorkSpecification {
        String title = "unknown";
        Set<String> tags = new HashSet<>();
        List<OnlineUrl> onlineUrls = new ArrayList<>();
    }

    @Data
    static class OnlineUrl {
        String url = "#";
        Integer pages = -1;
        String origin = "unknown";
        List<String> type = new ArrayList<>();
    }
}
