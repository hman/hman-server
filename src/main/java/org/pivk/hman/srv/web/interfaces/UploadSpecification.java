package org.pivk.hman.srv.web.interfaces;

import org.pivk.hman.srv.exception.ResourceBadRequestException;

import java.util.List;
import java.util.Set;

public interface UploadSpecification {
    void propagate();
    void validate() throws ResourceBadRequestException;

    Set<String> getTags();
    List<OnlineUrlSpecification> getOnlineUrls();

    String getTitle();

}
