//package org.pivk.hman.srv.web.service;
//
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//public class SearchParser {
//    public static String parseSearch(String value, SearchRequest request) throws SearchFormatException {
//        StringBuilder simpleSearch = new StringBuilder();
//        StringBuilder foundTags = new StringBuilder();
//
//
//        List<String> matchList = new ArrayList<>();
//        Pattern regex = Pattern.compile("[^\\s']+|'([^']*)'");
//        Matcher regexMatcher = regex.matcher(value);
//        while (regexMatcher.find()) {
//            String match = regexMatcher.group(0);
//            if(match.contains(":")){//tag search
//                if (!regexMatcher.find()) throw new SearchFormatException("Missing tag definition!\nFormat: (-)NAMESPACE:'TAG'");
//                String tagMatch = regexMatcher.group(1);
//                if(match.startsWith("-")){
//                    foundTags.append(match).append("'").append(tagMatch).append("' ");
//                    match = match.substring(1,match.length()-1);
//                    if(match.equalsIgnoreCase("tag")){ //exclude raw tag
//                        request.excludeRawTags(tagMatch);
//                    } else{ //exclude namespaced tag
//                        request.excludeTags(match+":"+tagMatch);
//                    }
//                } else{
//                    foundTags.append(match).append("'").append(tagMatch).append("' ");
//                    match = match.substring(0,match.length()-1);
//                    if(match.equalsIgnoreCase("tag")){ //include raw tag
//                        request.includeRawTags(tagMatch);
//                    } else{ //include namespaced tag
//                        request.includeTags(match+":"+tagMatch);
//                    }
//                }
//            } else{ // simple search
//                simpleSearch.append(match).append(" ");
//            }
//        }
//
//        String finalSearch = "";
//
//        if(simpleSearch.length() > 0) {
//            simpleSearch = new StringBuilder(simpleSearch.substring(0, simpleSearch.length() - 1));
//            finalSearch = simpleSearch.toString();
//            request.setSearch(simpleSearch.toString());
//        }
//
//        if(foundTags.length() > 0){
//            foundTags = new StringBuilder(foundTags.substring(0, foundTags.length() - 1));
//            if(simpleSearch.length() == 0){
//                finalSearch += foundTags;
//            } else{
//                finalSearch += " "+foundTags;
//            }
//        }
//
//        return finalSearch;
//    }
//}
