package org.pivk.hman.srv.jpamodel;

public enum PendingChangeType {
    TAG_DELETE, TAG_ADD
    , TITLE_SET
    , TYPE_MODIFY
    , PART_OF_SET
    , HAS_ADD
    , RESOURCE_DELETE, RESOURCE_ADD
    , COVER_SET
    , CREATED_SET, RELEASED_SET
    , UPLOAD_ARCHIVE
}