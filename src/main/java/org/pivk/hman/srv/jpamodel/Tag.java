package org.pivk.hman.srv.jpamodel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

import static javax.persistence.GenerationType.AUTO;

/**
 * Created by piv-k on 6/27/17.
 *
 * @since 0.1
 */
@Entity(name = "Tag")
//constraint: namespaced tag combination should only be contained once in the database
@Table(name = "tag", uniqueConstraints = @UniqueConstraint(columnNames = {"name","namespace"}))
@Getter @Setter @NoArgsConstructor
public class Tag {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="tag_id") @GeneratedValue(strategy = AUTO)
    private @Id Long tagId;

    // ---------------
    // COLUMNS
    // ---------------

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "namespace", nullable = false)
    private String namespace;
    @Column(name = "created", nullable = false)
    private Timestamp created = new Timestamp(new Date().getTime()); //db insertion time

    // ---------------
    // RELATIONS
    // ---------------

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Archive.class, cascade = CascadeType.ALL)
    @JoinTable(name = "archive_tag", joinColumns = { @JoinColumn(name = "tag_id") },
            inverseJoinColumns = { @JoinColumn(name = "archive_id") })
    private List<Archive> archives = new ArrayList<>();

    public Tag(String name){
        this.name = name;
        this.namespace = "tag";
    }

    public Tag(String namespace, String name){
        this.name = name;
        this.namespace = namespace;
    }

    @Override
    public String toString(){
        return namespace+":"+name;
    }
}