package org.pivk.hman.srv.jpamodel;

/**
 * @since 0.12
 */
public enum ArchiveType {
    COLLECTION, GALLERY, WORK, UNKNOWN
}
