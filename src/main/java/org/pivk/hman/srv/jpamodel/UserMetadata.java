package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by piv-k on 9/2/17.
 *
 * @since 0.12
 */
@Entity(name = "UserMetadata")
@Table(name = "user_metadata")
@Slf4j @Getter @Setter
public class UserMetadata {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="user_metadata_id") @GeneratedValue(strategy = GenerationType.AUTO)
    private @Id Long userMetadataId;

    // ---------------
    // COLUMNS
    // ---------------

    @Column(name = "rating"    , nullable = false)
    @DecimalMin("0.0") @DecimalMax("5.0")
    private BigDecimal rating = new BigDecimal(0.0); //default: unrated (0.0)

    @Column(name = "last_read" , nullable = false)
    @JsonInclude
    private Timestamp lastRead = new Timestamp(new Date(0).getTime()); //initially read: never

    @Column(name = "read_count", nullable = false)
    private Integer readCount = 0;

    @Column(name = "trash"     , nullable = false)
    private Boolean trash = false;

    @ElementCollection(fetch = FetchType.LAZY)
    @JsonInclude
    private List<String> favoriteGroups = new ArrayList<>();

    // ---------------
    // RELATIONS
    // ---------------

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Archive archive;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn
    private User user;

    @JsonIgnore
    public void patch(UserMetadata userMetadata) {
        if(userMetadata.getRating() != null) rating = userMetadata.getRating();
        if(userMetadata.getLastRead() != null) lastRead = userMetadata.getLastRead();
        if(userMetadata.getReadCount() != null) readCount = userMetadata.getReadCount();
        if(userMetadata.getTrash() != null) trash = userMetadata.getTrash();
        if(userMetadata.getFavoriteGroups() != null) favoriteGroups = userMetadata.getFavoriteGroups();
    }
}
