package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

import static javax.persistence.CascadeType.*;

/**
 * Created by piv-k on 7/9/17.
 *
 * @since 0.12
 */
@Entity(name = "Archive")
@Table(name = "archive")
@Slf4j @Getter @Setter @NoArgsConstructor
public class Archive {

    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="archive_id") @GeneratedValue(strategy = GenerationType.AUTO)
    private @Id Long archiveId;

    // ---------------
    // COLUMNS
    // ---------------
    @Column(name = "title", length = 1000, nullable = false) //titles can get very long
    private String title = "Unknown archive";

    @Column(name = "created", nullable = false)
    private Timestamp created = new Timestamp(new Date().getTime());

    @Column(name = "released")
    private Timestamp released;

    @Enumerated(EnumType.STRING)
    private ArchiveType type;

    // ---------------
    // RELATIONS
    // ---------------

    // one to one

    @OneToOne(mappedBy = "archive", fetch = FetchType.LAZY, optional = false, cascade = ALL)
    private Details details;

    @OneToOne(mappedBy = "archive", fetch = FetchType.LAZY, cascade = ALL) //null: no thumbnail
    private Cover cover;

    // one to many

    @JsonIgnore
    @OneToMany(mappedBy="archive", fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL)
    private List<Resource> resources = new ArrayList<>();

    @JsonIgnore
    @RestResource(exported = false)
    @OneToMany(mappedBy="archive", fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL)
    private List<UserMetadata> userMetadata = new ArrayList<>();

    @RestResource(exported = true)
    @OneToMany(mappedBy="archive", fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL)
    private List<PendingChange> pendingChanges = new ArrayList<>();

    // many to many

    @JsonIgnore
    @RestResource(exported = false)
    @ManyToMany(mappedBy = "archives", fetch = FetchType.LAZY, cascade = ALL)
    private List<Tag> tags = new ArrayList<>();

    // ---------------
    // SELF RELATION
    // ---------------

    //owner side, has sub archives
    @OneToMany(mappedBy = "archive", cascade = ALL, fetch = FetchType.LAZY)
    @RestResource(rel = "has")
    private List<Archive> archives = new ArrayList<>();
    //child side, belongs to one archive
    @JsonIgnore
    @RestResource(rel = "partOf")
    @ManyToOne
    private Archive archive;
    // order of the archive inside, null if top level
    @Column(name = "spot")
    private Integer spot;
}