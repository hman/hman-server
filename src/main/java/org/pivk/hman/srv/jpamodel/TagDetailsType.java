package org.pivk.hman.srv.jpamodel;

/**
 * @since 0.12
 */
public enum TagDetailsType {
    PAGE, TIME
}
