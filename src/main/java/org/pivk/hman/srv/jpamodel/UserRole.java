package org.pivk.hman.srv.jpamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

/** Created by piv-k on 9/12/17. */
@Entity(name = "UserRole")
@Table(name = "user_role")
@Slf4j @Getter @Setter
class UserRole {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name = "user_role_id") @GeneratedValue
    private @Id Long userRoleId;

    // ---------------
    // COLUMNS
    // ---------------
    @Column(name = "role", nullable = false)
    private String userRole;

    // ---------------
    // RELATIONS
    // ---------------

    @ManyToOne(fetch=FetchType.LAZY) @JoinColumn(name="user_role_user")
    private User user;
}
