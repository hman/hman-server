package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

/**
 * Created by piv-k on 6/27/17.
 *
 * @since 0.12
 */
@Entity(name = "TagDetails")
@Table(name = "tag_details")
@Getter @Setter @NoArgsConstructor
public class TagDetails {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="tag_details_id") @GeneratedValue(strategy = AUTO)
    private @Id Long tagDetailsId;

    // ---------------
    // COLUMNS
    // ---------------

    @Column(name = "tag_from", nullable = false)
    private String tagFrom;
    @Column(name = "tag_to", nullable = false)
    private String tagTo;
    @Enumerated(EnumType.STRING)
    private TagDetailsType type;

    // ---------------
    // RELATIONS
    // ---------------

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Archive archive;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Tag tag;
}