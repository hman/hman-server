package org.pivk.hman.srv.jpamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by piv-k on 7/2/17.
 *
 * @since 0.12
 */
@Entity(name = "Cover")
@Table(name = "cover")
@Slf4j @Getter @Setter
public class Cover {

    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name = "cover_id")
    private @Id Long coverId;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private Archive archive;

    // ---------------
    // COLUMNS
    // ---------------

    @Column(name = "url", nullable = false)
    private String url = "";

    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @Column(name = "hash", nullable = false)
    private String binaryHash = "0"; //initially 0, calculated asynchronously

    // ---------------
    // RELATIONS
    // ---------------
}