package org.pivk.hman.srv.jpamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

/**
 * Created by piv-k on 9/3/17.
 *
 * @since 0.12
 */
@Entity(name = "Details")
@Table(name = "details")
@Slf4j @Getter @Setter
public class Details {

    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="details_id")
    private @Id Long detailsId;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private Archive archive;

    // ---------------
    // COLUMNS
    // ---------------

    // ---------------
    // RELATIONS
    // ---------------

}
