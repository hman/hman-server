package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

/**
 * @since 0.12
 */
@Entity(name = "Vote")
@Table(name = "vote")
@Slf4j @Getter @Setter @NoArgsConstructor
public class Vote {

    @Column(name = "vote_id") @GeneratedValue(strategy = GenerationType.AUTO)
    private @Id Long voteId;

    @JsonIgnore //redundant because of @JsonValue
    @ManyToOne(fetch = FetchType.LAZY)
    private PendingChange change;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private User user;

    @JsonValue
    public String voteRepesenration(){
        String username = user.getUserName();
        String roles = "(";
        for (UserRole userRole : user.getUserDetails()) {
            String role = userRole.getUserRole();
            if(!role.equalsIgnoreCase("user")) {
                roles += role;
            }
        }
        if(!roles.equalsIgnoreCase("(")) {
            roles += ")";
        } else {
            roles = "";
        }

        return username+roles;
    }
}
