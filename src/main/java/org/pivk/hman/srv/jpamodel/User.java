package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pivk.hman.srv.jpamodel.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/** Created by piv-k on 9/12/17. */
@Entity(name = "User")
//constraint: namespaced tag combination should only be contained once in the database
@Table(name = "users")
@Getter @Setter @NoArgsConstructor
public class User {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name="user_id") @GeneratedValue
    private @Id Long userId;

    // ---------------
    // COLUMNS
    // ---------------

    @Column(name = "username", nullable = false, unique = true)
    private String userName;
    @JsonIgnore
    @Column(name = "password", nullable = false)
    private String password;
    @JsonIgnore
    @Column(name = "email", nullable = true, unique = true) //optional email
    private String email;
    @JsonIgnore
    @Column(name = "enabled", nullable = false)
    private Boolean enabled = true;
    @JsonIgnore
    @Column(name = "created", nullable = false)
    private Date created = new Date(); //db insertion time

    // ---------------
    // RELATIONS
    // ---------------

    @JsonIgnore
    @OneToMany(mappedBy="user", cascade = ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<UserRole> userDetails = new ArrayList<>();
}