package org.pivk.hman.srv.jpamodel;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * @since 0.1
 */
@Entity(name = "Resource")
@Table(name = "resource")
@Getter @Setter @NoArgsConstructor
public class Resource {
    // ---------------
    // PRIMARY KEY
    // ---------------

    @Column(name = "resource_id") @GeneratedValue
    private @Id Long resourceId;

    // ---------------
    // COLUMNS
    // ---------------

    @Enumerated(EnumType.STRING)
    private ResourceType type;

    @Column(name = "created", nullable = false)
    private Date created = new Date(); //upload timestamp after metadata fetched, initially db insertion time

    // ---------------
    // RELATIONS
    // ---------------

    @ManyToOne(fetch=FetchType.LAZY) @JoinColumn(name="archive_resource")
    private Archive archive;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn
    private User user;
}