package org.pivk.hman.srv.jpamodel;

/**
 * @since 0.12
 */
public enum ResourceType {
    BACKUP_URL, IMAGE_ARCHIVE, MOVIE
}
