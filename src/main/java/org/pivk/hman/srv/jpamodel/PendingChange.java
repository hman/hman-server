package org.pivk.hman.srv.jpamodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * @since 0.12
 */
@Entity(name = "PendingChange")
@Table(name = "pending_change", uniqueConstraints={@UniqueConstraint(columnNames = {"content" , "type", "archive_archive_id"})})
@Slf4j @Getter @Setter @NoArgsConstructor
public class PendingChange {

    @Column(name = "pending_change_id") @GeneratedValue(strategy = GenerationType.AUTO)
    private @Id Long pendingChangeId;

    @Enumerated(EnumType.STRING)
    private PendingChangeType type;

    @Column(name = "content")
    private String content;

    @Column(name = "reason")
    private String reason;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Archive archive;

    @OneToOne(cascade = ALL, fetch = FetchType.LAZY)
    @JoinColumn
    @JsonUnwrapped
    private User user;

    @OneToMany(mappedBy="change", fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL)
    @JsonUnwrapped
    private List<Vote> votes = new ArrayList<>();

}
